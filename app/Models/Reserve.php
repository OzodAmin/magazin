<?php

namespace App\Models;

use App\User;
use Eloquent as Model;
use App\Models\Currency;
use App\Models\Product;
use App\Models\ReservrTypes;

class Reserve extends Model
{
    const PRODUCT = 1;
    const COMISSION = 2;
    const FINE = 3;
    const MEHANIZM = 4;

	public $table = 'reserve';

	protected $casts = [
        'type_id' => 'integer', 
        'user_id' => 'integer',
        'currency_id' => 'integer',
        'product_id' => 'integer'
    ];

	protected $fillable = [
        'type_id', 
        'user_id',
        'product_id',
        'currency_id',
        'summa',
        'total',
        'prosent',
        'IsActive'
    ];

    public function type(){return $this->belongsTo(ReservrTypes::class, 'type_id', 'id');}
    public function user(){return $this->belongsTo(User::class, 'user_id', 'id');}
    public function product(){return $this->belongsTo(Product::class, 'product_id', 'id');}
    public function currency(){return $this->belongsTo(Currency::class, 'currency_id', 'id');}
}