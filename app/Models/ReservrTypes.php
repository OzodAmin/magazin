<?php

namespace App\Models;

use Eloquent as Model;

class ReservrTypes extends Model
{
    use \Dimsav\Translatable\Translatable;

    public $table = 'reserve_types';

    public $translationModel = 'App\Models\ReservrTypesTranslation';

    public $translatedAttributes = ['title'];

    public $timestamps = false;

    public static $rules = [
        'ru.title' => 'required|string|min:1|max:255',
        'uz.title' => 'required|string|min:1|max:255',
        'en.title' => 'required|string|min:1|max:255'
    ];

    protected static function boot() {
        parent::boot();

        static::deleting(function($category) {
            $category->deleteTranslations();
        });
    }
}