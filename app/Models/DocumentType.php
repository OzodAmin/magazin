<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class DocumentType extends Model
{
    use \Dimsav\Translatable\Translatable;

    public $table = 'document_type';

    public $translationModel = 'App\Models\DocumentTypeTranslation';

    public $translatedAttributes = [
        'name',
    ];

    public $timestamps = false;

    protected $fillable = ['code'];

    public static $rules = [
        'ru.name' => 'required|string|min:1|max:255',
        'uz.name' => 'required|string|min:1|max:255',
        'en.name' => 'required|string|min:1|max:255'
    ];

    protected static function boot() {
        parent::boot();

        static::deleting(function($item) {
            $item->deleteTranslations();
        });
    }
}