<?php

namespace App\Models;

use App\User;
use Eloquent as Model;
use App\Models\RequestModel;
use App\Models\Product;

class RequestUser extends Model
{
	public $table = 'request_user';
    public $timestamps = false;

	protected $casts = [
        'user_id' => 'integer', 
        'request_id' => 'integer',
        'product_id' => 'integer'
    ];

	protected $fillable = [
        'user_id', 
        'request_id',
        'product_id',
        'product_price'
    ];

    public static $rules = [
        'user_id' => 'required',
        'request_id' => 'required',
        'product_id' => 'required',
        'product_price' => 'required'
    ];

    public function user(){return $this->belongsTo(User::class, 'user_id', 'id');}
    public function request(){return $this->belongsTo(RequestModel::class, 'request_id', 'id');}
    public function product(){return $this->belongsTo(Product::class, 'product_id', 'id');}
}