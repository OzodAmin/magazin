<?php
namespace App\Models;

use Eloquent as Model;
use Entrust;

class SystemParams extends Model
{
    public $table = 'system_params';
    public $timestamps = false;

    protected $fillable = [
        'zadatok_seller',
        'zadatok_buyer',
        'comission_seller',
        'comission_buyer'
    ];

    public function getZalog()
    {
        $zalog = $this->find(1);
        if (Entrust::hasRole('seller'))
            return $zalog->zadatok_seller;
        else if (Entrust::hasRole('buyer'))
            return $zalog->zadatok_buyer;
        else
            return 0;
    }

    public function getComission()
    {
        $comission = $this->find(1);
        if (Entrust::hasRole('seller'))
            return $comission->comission_seller;
        else if (Entrust::hasRole('buyer'))
            return $comission->comission_buyer;
        else
            return 0;
    }
}