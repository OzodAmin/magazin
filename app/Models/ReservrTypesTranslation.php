<?php

namespace App\Models;

use Eloquent as Model;

class ReservrTypesTranslation extends Model {

    public $table = 'reserve_types_translations';

    public $timestamps = false;

    public $fillable = ['title'];

    public static $rules = ['title' => 'required|string|min:1'];
}
