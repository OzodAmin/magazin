<?php

namespace App\Models;

use Eloquent as Model;

class ContractStatus extends Model
{
    const NEW = '1';
    const SELLER_APPROVE = '2';
    const BUYER_APPROVE = '3';
    const APPROVED = '4';

    use \Dimsav\Translatable\Translatable;

    public $table = 'contract_statuses';

    public $translationModel = 'App\Models\ContractStatusTranslation';

    public $timestamps = false;

    public $translatedAttributes = ['name'];

    public static $rules = [
        'ru.name' => 'required|string|min:3|max:255',
        'uz.name' => 'required|string|min:3|max:255',
        'en.name' => 'required|string|min:3|max:255',
    ];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($city) {
            $city->deleteTranslations();
        });
    }
}