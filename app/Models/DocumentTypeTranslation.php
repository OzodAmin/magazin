<?php

namespace App\Models;

use Eloquent as Model;

class DocumentTypeTranslation extends Model {

    public $table = 'document_type_translations';

    public $timestamps = false;

    public $fillable = ['name'];

    public static $rules = ['name' => 'required|string|min:1'];
}
