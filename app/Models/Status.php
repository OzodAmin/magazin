<?php
namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Status extends Model
{
    const IN_MODERATION_NEW = '1';
    const ACTIVE = '2';
    const NO_PAID_COMMISSION = '4';
    const BLOCKED = '7';
    const IN_MODERATION_EDITED = '8';
    const DEACTIVATED = '9';

    use \Dimsav\Translatable\Translatable;

    public $table = 'statuses';

    public $translationModel = 'App\Models\StatusTranslation';

    public $timestamps = false;

    public $translatedAttributes = ['name'];

    public static $rules = [
        'ru.name' => 'required|string|min:3|max:255',
        'uz.name' => 'required|string|min:3|max:255',
        'en.name' => 'required|string|min:3|max:255',
    ];

    protected static function boot() {
        parent::boot();

        static::deleting(function($city) {
            $city->deleteTranslations();
        });
    }
}
