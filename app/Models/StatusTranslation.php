<?php

namespace App\Models;

use Eloquent as Model;

class StatusTranslation extends Model
{
    public $table = 'status_translations';

    public $timestamps = false;

    public $fillable = ['name'];

    public static $rules = ['name' => 'required|string|min:3'];
}