<?php
namespace App\Models;

use App\User;
use App\Models\Currency;
use Eloquent as Model;
use App\Models\Basis;
use App\Models\ContractStatus;
use App\Models\City;
use App\Models\Product;
use App\Models\Measure;

class Contract extends Model
{
    public $table = 'contracts';

    protected $casts = [
        'basis_id' => 'integer', 
        'city_id' => 'integer',
        's_client_id' => 'integer',
        'currency_id' => 'integer',
        'product_id' => 'integer',
        'measure_id' => 'integer',
        'status' => 'integer'
    ];

    protected $fillable = ['measure_id', 'product_price', 'quantity', 'product_id', 'docN', 'depositP', 'srokDate', 'basis_id', 'city_id', 's_client_id', 's_inn', 's_bank_name', 's_bank_account', 's_bank_code', 's_dutyP', 'b_client_id', 'b_inn', 'b_bank_name', 'b_bank_account', 'b_bank_code', 'b_butyP', 'summa', 'currency_id', 'status', 'annul_date', 'annul_reason', 'annul_user_id', 'file', 'notes', 'pay_date'];

    public function basis(){return $this->belongsTo(Basis::class, 'basis_id', 'id');}
    public function statusTable(){return $this->belongsTo(ContractStatus::class, 'status', 'id');}
    public function city(){return $this->belongsTo(City::class, 'city_id', 'id');}
    public function seller(){return $this->belongsTo(User::class, 's_client_id', 'id');}
    public function buyer(){return $this->belongsTo(User::class, 'b_client_id', 'id');}
    public function annualUser(){return $this->belongsTo(User::class, 'annul_user_id', 'id');}
    public function currency(){return $this->belongsTo(Currency::class, 'currency_id', 'id');}
    public function product(){return $this->belongsTo(Product::class, 'product_id', 'id');}
    public function measure(){return $this->belongsTo(Measure::class, 'measure_id', 'id');}
}