<?php

namespace App\Models;

use Eloquent as Model;

class ContractStatusTranslation extends Model
{
    public $table = 'contract_status_translations';

    public $timestamps = false;

    public $fillable = ['name'];

    public static $rules = ['name' => 'required|string|min:3'];
}