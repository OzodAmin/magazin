<?php
namespace App\Models;

use App\User;
use App\Models\City;
use Eloquent as Model;
use App\Models\Product;
use App\Models\RequestUser;

class RequestModel extends Model
{
    const NEW = '1';
    const EXPIRED = '2';

	public $table = 'request';

	protected $casts = [
        'user_id' => 'integer', 
        'product_id' => 'integer',
        'city_id' => 'integer',
        'status' => 'integer'
    ];

	protected $fillable = [
        'user_id', 
        'product_id',
        'city_id',
        'status',
        'product_quantity',
        'comment',
        'pay_date',
        'expire_at'
    ];

    public static $rules = [
        'user_id' => 'required',
        'product_id' => 'required',
        'city_id' => 'required',
        'status' => 'required',
        'product_quantity' => 'required',
        'comment' => 'required',
        'pay_date' => 'required',
        'expire_at' => 'required'
    ];

    public function user(){return $this->belongsTo(User::class, 'user_id', 'id');}
    public function product(){return $this->belongsTo(Product::class, 'product_id', 'id');}
    public function city(){return $this->belongsTo(City::class, 'city_id', 'id');}

    public function requestUsers(){return $this->hasMany(RequestUser::class, 'request_id', 'id');}
}