<?php
namespace App\Http\Controllers\Admin;

use App\Models\ContractStatus;
use Illuminate\Http\Request;

class ContractStatusesController
{
    public function index(Request $request)
    {
        $contractStatuses = ContractStatus::paginate(15);
        return view('admin.contractStatuses.index',compact(['contractStatuses']));
    }
}