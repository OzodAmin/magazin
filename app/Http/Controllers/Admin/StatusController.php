<?php

namespace App\Http\Controllers\Admin;

use Flash;
use App\Models\Status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StatusController extends Controller
{
    public function index()
    {
        $statuses = Status::whereTranslation('locale', 'ru')->paginate(10);
        return view('admin.statuses.index',compact('statuses'));
    }

    public function edit($id)
    {
        $status = Status::findOrFail($id);
        return view('admin.statuses.edit',compact('status'));
    }

    
    public function update(Request $request, $id)
    {
        $this->validate($request, [Status::$rules]);

        $status = Status::findOrFail($id);
        if (empty($status)) {
            Flash::error('Status not found');
            return redirect(route('statuses.index'));
        }

        $status->update($request->except(['_token', '_method'])); 

        Flash::success('Status updated successfully.');

        return redirect(route('statuses.index'));   
    }

    public function create(){}
    public function store(Request $request){}
    public function show($id){}
    public function destroy($id){}
}
