<?php

namespace App\Http\Controllers\Admin;

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use App\Http\Controllers\AppBaseController;
use Intervention\Image\Facades\Image;
use Illuminate\Http\UploadedFile;
use App\Models\Status;
use App\Models\ChildCategory;
use Illuminate\Http\Request;
use App\Models\Holidays;
use App\Models\Category;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Country;
use App\Models\Measure;
use App\Models\Basis;
use Carbon\Carbon;
use App\User;
use Flash;
use DB;

class ProductController extends AppBaseController
{
    public function index(Request $request)
    {
        $products = Product::whereTranslation('locale', 'ru')->
                    orderBy('status', 'asc');


        if ($request->has('name') && $request->name !== null) {
            $products->whereTranslationLike('title', '%'.$request->name.'%');
        }

        if ($request->has('status') && $request->status !== null) {
            $products->where('status', $request->status);
        }
        if ($request->has('main_category') && $request->main_category !== null) {
            $products->where('parent_category_id', $request->main_category);
        }
        if ($request->has('child_category') && $request->child_category !== null) {
            $products->where('child_category_id', $request->child_category);
        }

        $statuses = Status::whereTranslation('locale', 'ru')->get();
        $statusesArray = [];
        foreach($statuses as $item) {$statusesArray[$item->id] = $item->name;}

        $categories = Category::whereTranslation('locale', 'ru')->get();
        $categoriesArray = [];
        foreach($categories as $item) {$categoriesArray[$item->id] = $item->title;}

        $categoriesChild = ChildCategory::whereTranslation('locale', 'ru')->
        where('category_id', $request->main_category)->
        get();
        $categoriesChildArray = [];
        foreach($categoriesChild as $item) {$categoriesChildArray[$item->id] = $item->title;}



        $products = $products->
        paginate(15)->appends($request->all());


        return view('admin.products.index',compact(['products', 'categoriesArray', 'categoriesChildArray', 'statusesArray', 'request']));
    }

    public function edit($id)
    {
        $product = Product::where('id', $id)->first();

        if (empty($product)) {
            return redirect(route('products.index'))->with('data', 'Product not found');
        }

        $categories = Category::whereTranslation('locale', 'ru')->get();
        $categoriesArray = [];
        foreach($categories as $item) {$categoriesArray[$item->id] = $item->title;}

        $categoriesChild = ChildCategory::whereTranslation('locale', 'ru')->
                                            where('category_id', $product->parent_category_id)->
                                            get();
        $categoriesChildArray = [];
        foreach($categoriesChild as $item) {$categoriesChildArray[$item->id] = $item->title;}

        $measures = Measure::whereTranslation('locale', 'ru')->get();
        $measuresArray = [];
        foreach($measures as $item) {$measuresArray[$item->id] = $item->title;}

        $currency = Currency::get();
        $currencyArray = [];
        foreach($currency as $item) {$currencyArray[$item->id] = $item->code;}

        $basis = Basis::get();
        $basisArray = [];
        foreach($basis as $item) {$basisArray[$item->id] = $item->code;}

        $country = Country::whereTranslation('locale', 'ru')->get();
        $countryArray = [];
        foreach($country as $item) {$countryArray[$item->id] = $item->title;}

        $statuses = Status::whereTranslation('locale', 'ru')->get();
        $statusesArray = [];
        foreach($statuses as $item) {$statusesArray[$item->id] = $item->name;}

        if ($product->status == Status::NO_PAID_COMMISSION) {
            $message = parent::reserveMoney($product, $product->user_id);
            if($message)
                Flash::error($message);
            else{
                $product->status = Status::IN_MODERATION_NEW;
                $product->save();
                Flash::success('Комисионный сбор и Депозить успешно снято');
            }
        }

        return view('admin.products.edit', compact(['product','categoriesArray', 'measuresArray', 'currencyArray', 'basisArray', 'countryArray', 'statusesArray', 'categoriesChildArray']));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, Product::$rules);

        $product = Product::where('id', $id)->first();

        if (empty($product)) {
            Flash::error('Product not found');
            return redirect(route('products.index'));
        }

        if ($product->status == Status::IN_MODERATION_NEW && $request['status'] == Status::ACTIVE) {
            $product->updated_at = Carbon::now();
            $holidays = Holidays::where('holiday', '>', Carbon::now())->get();
            $holidays = $holidays->toArray();

            $MyDateCarbon = Carbon::now();
            $MyDateCarbon->addWeekdays(10);

            for ($i = 1; $i <= 10; $i++) {

                if (in_array(Carbon::now()->addWeekdays($i)->toDateString(), $holidays)) {

                    $MyDateCarbon->addDay();

                }
            }

            $request->merge(['expire_at' => $MyDateCarbon]);
        }

        $product->fill($request->except(['featured_image']));

        if ($request->hasFile('featured_image')) {
            $this->removePhoto( $product->featured_image );
            $product->featured_image = $this->uploadPhoto( $request->file('featured_image') );
        }

        $product->save();

        Flash::success('Product updated successfully.');
        return redirect(route('products.index'));
    }


    public function destroy($id)
    {
        $product = Product::where('id', $id)->first();

        if (empty($product)) {
            Flash::error('Product not found');
            return redirect(route('products.index'));
        }

        $product->status = Status::BLOCKED;
        $product->save();

        Flash::success('Product frozen successfully.');

        return redirect(route('products.index'));
    }

    protected function uploadPhoto(UploadedFile $file) {

        $base_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $image_name = str_slug($base_name, '_').'_'.time().'.'.$file->getClientOriginalExtension();

        $file->move(public_path('uploads/product/'), $image_name);

        Image::make(public_path('uploads/product/').$image_name)
            ->fit(100)
            ->save(public_path('uploads/product/').'admin_'.$image_name);

        Image::make(public_path('uploads/product/').$image_name)
            ->fit(720, 960)
            ->save(public_path('uploads/product/').'thumb_'.$image_name);

        Image::make(public_path('uploads/product/').$image_name)
            ->fit(1200, 1600)
            ->save(public_path('uploads/product/').'icon_'.$image_name);

        return $image_name;
    }

    protected function removePhoto( $image_name ) {

        if( !empty($image_name) ) {
            unlink( public_path('uploads/product/').$image_name );
            unlink( public_path('uploads/product/admin_').$image_name );
            unlink( public_path('uploads/product/thumb_').$image_name );
            unlink( public_path('uploads/product/icon_').$image_name );
        }
    }
}
