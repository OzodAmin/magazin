<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppBaseController;
use App\User;
use Illuminate\Http\Request;
use App\Models\Contract;

class ContractsController extends AppBaseController
{
    public function index(Request $request)
    {
        $contracts = Contract::orderBy('status', 'asc');


        if ($request->has('date') && $request->date !== null) {
            $contracts->where('created_at','like', '%' . $request->date . '%');
        }

        if ($request->has('summa') && $request->summa !== null) {
            $contracts->where('summa','like', '%' . $request->summa . '%');
        }

        if ($request->has('buyer') && $request->buyer !== null) {
            $contracts->where('b_client_id', $request->buyer);
        }
        if ($request->has('seller') && $request->seller !== null) {
            $contracts->where('s_client_id', $request->seller);
        }
        if ($request->has('contract_number') && $request->contract_number !== null) {
            $contracts->where('docN', $request->contract_number);
        }
        $buyers = User::whereNotNull('company_legal_name')->where('username', 'like', 'Z%')->get();
        $buyerArray = [];
        foreach($buyers as $item) {$buyerArray[$item->id] = $item->company_legal_name;}

        $sellers = User::whereNotNull('company_legal_name')->where('username', 'like', 'U%')->get();
        $sellerArray = [];
        foreach($sellers as $item) {$sellerArray[$item->id] = $item->company_legal_name;}

        $contracts = $contracts->paginate(15)->appends($request->all());
        return view('admin.contracts.index',compact(['contracts', 'request', 'buyerArray', 'sellerArray']));

    }
}
