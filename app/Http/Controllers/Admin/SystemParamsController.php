<?php

namespace App\Http\Controllers\Admin;

use Flash;
use Illuminate\Http\Request;
use App\Models\SystemParams;
use App\Http\Controllers\Controller;

class SystemParamsController extends Controller
{
    public function index()
    {
        $params = SystemParams::find(1);
        return view('admin.params.index', compact(['params']));
    }

    public function update(Request $request)
    {
        $param = SystemParams::find(1);

        if (empty($param)) {
            Flash::error('Param not found');
            return redirect(url('backend'));
        }

        $param->zadatok_seller = $request['zadatok_seller'];
        $param->zadatok_buyer = $request['zadatok_buyer'];
        $param->comission_seller = $request['comission_seller'];
        $param->comission_buyer = $request['comission_buyer'];
        $param->save();

        Flash::success('Category updated successfully.');

        return redirect(url('backend'));
    }
}
