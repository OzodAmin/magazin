<?php

namespace App\Http\Controllers\Admin;

use function GuzzleHttp\Promise\all;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Http\Controllers\AppBaseController;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Mail;
use App\Mail\RegisterApprovedMail;
use App\Models\Status;
use App\User;
use App\Role;
use Flash;
use Hash;
use DB;

class UserController extends AppBaseController
{
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    public function index(Request $request)
    {
        $this->userRepository->pushCriteria(new RequestCriteria($request));

        $users = User::whereNotNull('status');


        if ($request->has('status') && $request->status !== null) {
            $users->where('status', $request->status);
        }
        if ($request->has('email') && $request->email !== null) {
            $users->where('email', 'like', '%' . $request->email . '%');
        }
        if ($request->has('name') && $request->name !== null) {
            $users->where('name', 'like', '%' . $request->name . '%');
        }
        if ($request->has('company') && $request->company !== null) {
            $users->where('company_legal_name', 'like', '%' . $request->company . '%');
        }

        if ($request->has('role') && $request->role !== null) {
            $users->leftJoin('role_user', function($join){
                $join->on('id', '=', 'role_user.user_id');
            })
                ->where('role_user.role_id', $request->role);
        }

        $statuses = Status::whereTranslation('locale', 'ru')->get();
        $statusesArray = [];
        foreach ($statuses as $status) {
            $statusesArray[$status->id] = $status->name;
        }
        $roles = Role::get();
        $rolesArray = [];
        foreach ($roles as $roles) {
            $rolesArray[$roles->id] = $roles->display_name;
        }

        $users = $users->paginate(15)->appends($request->all());


        return view('admin.users.index', compact(['users', 'request', 'statusesArray', 'rolesArray']));

    }

    public function create()
    {
        return redirect(route('users.index'));
    }

    public function store(Request $request)
    {
        return redirect(route('users.index'));
    }

    public function show($id)
    {
        $user = User::find($id);

        if (empty($user)) {
            Flash::error('User not found');
            return redirect(route('users.index'));
        }

        return view('admin.users.show', compact('user'));
    }

    public function edit($id)
    {
        $user = User::find($id);

        $statuses = Status::whereTranslation('locale', 'ru')->get();

        $statusArray = [];

        foreach ($statuses as $city) {
            $statusArray[$city->id] = $city->name;
        }

        return view('admin.users.edit', compact('user', 'statusArray'));
    }

    public function update(Request $request, $id)
    {
        //$this->validate($request, [User::$rules]);

        $user = User::find($id);

        if (empty($user)) {
            Flash::error('User not found');
            return redirect(route('users.index'));
        }

        $input = $request->except(['_method', '_token']);

        if ($user->status == 1 && $request['status'] == 2) {

            $password = str_random(8);
            $user->password = $password;
            Mail::to($user->email)->send(new RegisterApprovedMail($user));
            $request->merge([
                'password' => Hash::make($password),
                'email_verified_at' => Carbon::now()
            ]);

        }

        $input = $request->except(['_method', '_token']);
        $user->update($input);

        Flash::success('User updated successfully.');

        return redirect(route('users.index'));
    }

    public function destroy($id)
    {
        $user = User::find($id);

        if (empty($user)) {
            Flash::error('User not found');
            return redirect(route('users.index'));
        }

        $user->status = 7;
        $user->update();

        Flash::success('Статус пользователя ' . $user->name . ': "Заблокирован сотрудником РКП".');

        return redirect(route('users.index'));
    }
}
