<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppBaseController;
use Intervention\Image\Facades\Image;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\Request;
use App\Models\RkpBanks;
use App\Models\RkpPayTypes;
use App\Models\Status;
use App\Models\Currency;
use App\Models\Rkp;
use App\User;
use Flash;
use DB;

class RkpBackendController extends AppBaseController
{
    public function index(Request $request)
    {


        $rkps = Rkp::orderBy('status_id', 'asc');


        if ($request->has('status') && $request->status !== null) {
            $rkps->where('status_id', $request->status);
        }
        if ($request->has('saldo') && $request->saldo !== null) {
            $rkps->where('saldo','like', '%' . $request->saldo . '%');
        }
        if ($request->has('bank_name') && $request->bank_name !== null) {
            $rkps->where('bank_name', 'like', '%' . $request->bank_name . '%');
        }

        if ($request->has('company') && $request->company !== null) {
            $rkps->leftJoin('users', function($join){
                $join->on('user_id', '=', 'users.id');
            })
                ->where('users.id', $request->company);
        }
        if ($request->has('currency') && $request->currency !== null) {
            $rkps->leftJoin('currency', function($join){
                $join->on('currency_id', '=', 'currency.id');
            })
                ->where('currency.id', $request->currency);
        }


        $users = User::whereNotNull('company_legal_name')->get();
        $userArray = [];
        foreach($users as $item) {$userArray[$item->id] = $item->company_legal_name;}

        $currency = Currency::whereTranslation('locale', 'ru')->get();
        $currencyArray = [];
        foreach($currency as $item) {$currencyArray[$item->id] = $item->code;}

        $statuses = Status::whereTranslation('locale', 'ru')->get();
        $statusArray = [];
        foreach($statuses as $city) {$statusArray[$city->id] = $city->name;}


        $rkps = $rkps->paginate(15)->appends($request->all());

        return view('admin.rkps.index',compact(['rkps','currencyArray', 'statusArray', 'userArray', 'request']));
    }

    public function show($id){return redirect(route('rkpsAdmin.index'));}

    public function create(){return redirect(route('rkpsAdmin.index'));}

    public function edit($id)
    {
        $rkp = Rkp::where('id', $id)->first();

        if (empty($rkp)) {
            Flash::error('RKP Account not found');
            return redirect(route('rkpsAdmin.index'));
        }

        $userCurrency = DB::table('rkp')
                        ->select('currency_id')
                        ->where('user_id', $rkp->user_id)
                        ->get();
        $userCurrencyArray = [];
        foreach($userCurrency as $item) {$userCurrencyArray[] = $item->currency_id;}

        $diff = array_diff($userCurrencyArray, array($rkp->currency_id));

        $currency = Currency::whereNotIn('id', $diff)->get();
        $currencyArray = [];
        foreach($currency as $item) {$currencyArray[$item->id] = $item->code;}

        $statuses = Status::whereTranslation('locale', 'ru')->get();
        $statusArray = [];
        foreach($statuses as $city) {$statusArray[$city->id] = $city->name;}

        $banks = RkpBanks::whereTranslation('locale', 'ru')->get();
        $banksArray = [];
        foreach($banks as $item) {$banksArray[$item->id] = $item->bank_name;}

        return view('admin.rkps.edit', compact(['rkp', 'currencyArray', 'statusArray', 'banksArray']));
    }

    public function update($id, Request $request)
    {
        $rkp = Rkp::where('id', $id)->first();

        if (empty($rkp)) {
            Flash::error('RKP Account not found');
            return redirect(route('rkpsAdmin.index'));
        }

        $rkp->fill($request->except(['featured_image']));

        $rkp->save();

        Flash::success('RKP updated successfully.');
        return redirect(route('rkpsAdmin.index'));
    }
}
