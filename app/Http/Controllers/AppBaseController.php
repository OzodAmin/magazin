<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Bus\DispatchesJobs;

use InfyOm\Generator\Utils\ResponseUtil;
use App\Models\Product;
use App\Models\Reserve;
use App\Models\Holidays;
use App\Models\Rkp;
use Carbon\Carbon;
use Response;
use Auth;


class AppBaseController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $currentUser;

    public function currentUser(){
        $user = \Auth::user();
        $permissions = $user->getAllPermissions();
        return $permissions;
    }

    public function sendResponse($result, $message)
    {
        return Response::json(ResponseUtil::makeResponse($message, $result));
    }

    public function sendError($error, $code = 404)
    {
        return Response::json(ResponseUtil::makeError($error), $code);
    }

}
