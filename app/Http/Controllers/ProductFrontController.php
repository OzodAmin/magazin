<?php

namespace App\Http\Controllers;

use App\Helper;
use App\Models\Country;
use App\Models\Measure;
use App\Models\Product;
use App\Models\RequestModel;
use App\Models\RequestUser;
use App\Models\Reserve;
use App\Models\Status;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Redirect;

class ProductFrontController extends AppBaseController
{
    private $locale;

    public function __construct()
    {
        $this->locale = LaravelLocalization::getCurrentLocale();
    }

    public function index(Request $request)
    {
        $products = Product::whereTranslation('locale', $this->locale)->
        where('status', Status::ACTIVE)->
        where('expire_at', '>', Carbon::now());

        if ($request->has('manufacturer_country_id') && $request->manufacturer_country_id !== null) {
            $products->where('manufacturer_country_id', $request->manufacturer_country_id);
        }
        if ($request->has('lot_number') && $request->lot_number !== null) {
            $products->where('lot_number', $request->lot_number);
        }
        if ($request->has('produced_year') && $request->produced_year !== null) {
            $products->where('produced_year', '=', $request->produced_year);
        }
        if ($request->has('manufacturer_title') && $request->manufacturer_title !== null) {
            $products->where('manufacturer_title', $request->manufacturer_title);
        }
        if ($request->has('measure_id') && $request->measure_id !== null) {
            $products->where('measure_id', $request->measure_id);
        }
        if ($request->has('quantity_min') && $request->quantity_min !== null) {
            $products->where('quantity', '>=', $request->quantity_min);
        }
        if ($request->has('quantity_max') && $request->quantity_max !== null) {
            $products->where('quantity', '<=', $request->quantity_max);
        }

        $products = $products->
        paginate(16)->appends($request->all());

        $measures = Measure::whereTranslation('locale', $this->locale)->get();
        $measuresArray = [];
        foreach ($measures as $item) {
            $measuresArray[$item->id] = $item->title;
        }

        $country = Country::whereTranslation('locale', $this->locale)->get();
        $countryArray = [];
        foreach ($country as $item) {
            $countryArray[$item->id] = $item->title;
        }

        return view('product.index', compact(['products', 'request', 'measuresArray', 'countryArray']));
    }

    public function show($slug)
    {
        $product = Product::whereTranslation('locale', $this->locale)
            ->whereTranslation('slug', $slug)
            ->where('status', Status::ACTIVE)
            ->where('expire_at', '>', Carbon::now())
            ->first();

        if (empty($product))
            abort(404);

        return view('product.show', compact(['product']));
    }

    public function priceRequest(Request $request2)
    {
        if (auth()->id()) {
            $product = Product::where('id', $request2->productId)->first();
            $helper = new Helper();

            if (empty($product))
                return redirect(url('/'))->withErrors(['Product not found']);

            $quantity = $request2->owner_product_quantity;

            if ($quantity > $product->quantity)
                return Redirect::back()->withErrors(['Треб. кол-во превышает остатка у продавца'])->withInput();
            else {
                //Reserve money from user
                $message = $helper->reserveMoney($product, Reserve::MEHANIZM);
                if ($message)
                    return Redirect::back()->withErrors([$message])->withInput();

                $product->quantity = $product->quantity - $quantity;
                $product->save();
            }

            $expireDate = $helper->workingDays(2);

            $request = new RequestModel;
            $request->user_id = auth()->id();
            $request->city_id = $request2->cityId;
            $request->product_id = $request2->productId;
            $request->product_quantity = $quantity;
            $request->comment = $request2->comment;
            $request->pay_date = $request2->pay_date;
            $request->expire_at = $expireDate;
            $request->save();

            $requestUser = new RequestUser;
            $requestUser->request_id = $request->id;
            $requestUser->user_id = $product->user_id;
            $requestUser->product_id = $product->id;
            $requestUser->product_price = $product->price;
            $requestUser->save();

            $usersIds = DB::table('product')
                ->rightJoin('product_regions', 'product.id', 'product_regions.product_id')
                ->where('product.child_category_id', $product->child_category_id)
                ->where('product_regions.city_id', $request2['cityId'])
                ->select('product.user_id')
                ->get();

            $arrayUser = array(auth()->id(), $product->user_id);
            foreach ($usersIds as $value) {
                $userID = get_object_vars($value);
                if (!in_array($userID["user_id"], $arrayUser)) {
                    $arrayUser[] = $userID["user_id"];
                    $requestUser = new RequestUser;
                    $requestUser->request_id = $request->id;
                    $requestUser->user_id = $userID["user_id"];
                    $requestUser->save();
                }
            }

            return redirect(url('/'))->with('success', 'Mehanizm success');
        }

        abort(403, 'Unauthorized action.');
    }

    public function priceOffer(Request $request)
    {
        $product = Product::where('id', $request->productId)->first();
        $requestModel = RequestUser::where('request_id', $request->requestId)->where('user_id', auth()->id())->first();

        if ($product->quantity < $request->requiredQuantity)
            return Redirect::back()->withErrors(["Не достаточно товара на складе"])->withInput();

        $requestModel->product_id = $request->user_product_id;
        $requestModel->product_price = $request->user_product_price;
        $requestModel->save();

        return redirect(url('/'))->with('success', 'Offer accepted');
    }

    public function showByCategory($id)
    {
        $products = Product::whereTranslation('locale', $this->locale)->
        where('child_category_id', $id)->
        where('status', Status::ACTIVE)->
        where('expire_at', '>', Carbon::now())->
        paginate(16);
        return view('product.index', compact(['products']));
    }
}
