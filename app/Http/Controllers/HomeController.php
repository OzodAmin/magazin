<?php

namespace App\Http\Controllers;

use App\Mail\RegisterMail;
use App\Models\Category;
use App\Models\Product;
use App\Models\Status;
use App\User;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;
use Mail;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

// use App\Models\Product;
// use Carbon\Carbon;
// use App\Models\Status;

class HomeController extends AppBaseController
{
    private $locale;

    public function __construct()
    {
        $this->locale = LaravelLocalization::getCurrentLocale();
    }

    public function index(Request $request)
    {
//        $id = 0;
//        $lastNumber = 1;
//        $date = Carbon::now();
//
//        $requests = RequestModel::where('expire_at', '<', $date)->where('status', 1)->get();
//
//        $lastContract = Contract::orderBy('id', 'DESC')->first();
//
//        if (isset($lastContract)) {
//            $lastContractDate = Carbon::parse($lastContract->created_at);
//            $lastNumber = $lastContractDate->day;
//
//            if ($date->day == $lastNumber)
//                $lastNumber++;
//        }
//
//        foreach ($requests as $request) {
//
//            $requestUsers = RequestUser::where('request_id', $request->id)->orderBy('product_price')->orderBy('created_at')->get();
//
//            foreach ($requestUsers as $requestUser) {
//
//                if (!empty($requestUser->product_price) && isset($requestUser->product_price)) {
//                    if ($requestUser->request_id != $id) {
//                        $id = $requestUser->request_id;
//
//                        $summa = $request->product_quantity * $requestUser->product->price;
//                        $summa = round($summa, 2, PHP_ROUND_HALF_DOWN);
//
//                        $currency = $request->product->currency_id;
//
//                        $sellerBank = Rkp::where('currency_id', $currency)
//                                            ->where('user_id', $requestUser->user_id)
//                                            ->where('status_id', Status::ACTIVE)
//                                            ->first();
//
//                        $buyerBank = Rkp::where('currency_id', $currency)
//                                            ->where('user_id', $request->user_id)
//                                            ->where('status_id', Status::ACTIVE)
//                                            ->first();
//
//                        $contractNumber = $lastNumber . '-' . $date->format('dmy');
//                        $sellerBankName = $sellerBank['bank_name'];
//                        $sellerBankAccount = $sellerBank['bank_account'];
//                        $sellerBankCode = $sellerBank['bank_code'];
//                        $buyerBankName = $buyerBank['bank_name'];
//                        $buyerBankAccount = $buyerBank['bank_account'];
//                        $buyerrBankCode = $buyerBank['bank_code'];
//
//                        $contract = new Contract();
//                        $contract->docN = $contractNumber;
//                        $contract->product_id = $requestUser->product_id;
//                        $contract->product_price = $requestUser->product_price;
//                        $contract->quantity = $request->product_quantity;
//                        $contract->depositP = $requestUser->product->deposit;
//                        $contract->srokDate = $requestUser->product->basis_day;
//                        $contract->basis_id = $requestUser->product->basis_id;
//                        $contract->city_id = $request->city_id;
//                        $contract->s_client_id = $requestUser->user_id;
//                        $contract->s_inn = $requestUser->user->inn;
//                        $contract->s_bank_name = $sellerBankName;
//                        $contract->s_bank_account = $sellerBankAccount;
//                        $contract->s_bank_code = $sellerBankCode;
//                        $contract->s_dutyP = 0.15;
//                        $contract->b_client_id = $request->user_id;
//                        $contract->b_inn = $buyerBank->user->inn;
//                        $contract->b_bank_name = $buyerBankName;
//                        $contract->b_bank_account = $buyerBankAccount;
//                        $contract->b_bank_code = $buyerrBankCode;
//                        $contract->b_butyP = 0.15;
//                        $contract->summa = $summa;
//                        $contract->pay_date = $request->pay_date;
//                        $contract->currency_id = $currency;
//                        $contract->status = Status::ACTIVE;
//                        $contract->save();
//
//                        $request->status = RequestModel::EXPIRED;
//                        $request->save();
//                        $lastNumber++;
//                    }else{
//                        $product = Product::where('id', $requestUser->product_id)->first();
//                        $product->quantity += $request->product_quantity;
//                         $product->save();
//                    }
//                }
//            }
//        }


        $products = Product::whereTranslation('locale', '"'.$this->locale.'"')
            ->orderBy('id', 'desc')
            ->where('status', Status::ACTIVE)
            ->where('expire_at', '>', Carbon::now())
            ->take(5)
            ->get();

        $categories = Category::whereTranslation('locale', $this->locale)->get();
        return view('home.home', compact(['categories', 'products']));
    }

    public function contractProve(Request $request)
    {
        if (!$request->hasValidSignature()) {
            abort(401);
        }
        echo "string";
    }

    public function categoryPage()
    {
        $categories = Category::whereTranslation('locale', $this->locale)->get();
        return view('home.categories', compact(['categories']));
    }

    public function registeration(Request $request)
    {
        $this->validate($request, User::$rules);

        $request->merge([
            'status' => Status::IN_MODERATION_NEW,
            'password' => str_random(8)
        ]);

        if ($request['role'] == 8) {
            $request->merge(['username' => 'U' . $request['inn']]);
        } elseif ($request['role'] == 9) {
            $request->merge(['username' => 'Z' . $request['inn']]);
        }

        $password = $request['password'];
        $request['password'] = Hash::make($request['password']);
        $input = $request->except(['remember_token', 'role']);
        $user = User::create($input);

        $user->attachRole($request['role']);

        Mail::to($user->email)->send(new RegisterMail());

        return back()->with('success', 'Registration success');
    }

    public function profile()
    {
        $user = User::find(auth()->id());

        if (empty($user)) {
            return abort(404);
        }

        return view('dashboard.profile.profileShow', compact('user'));
    }

    public function profileEdit()
    {
        $user = User::find(auth()->id());

        if (empty($user)) {
            return abort(404);
        }

        return view('dashboard.profile.profileEdit', compact('user'));
    }
}
