<?php
namespace App\Http\Controllers\Dashboard;

use App\Helper;
use App\Http\Controllers\AppBaseController;
use App\Models\Basis;
use App\Models\Category;
use App\Models\ChildCategory;
use App\Models\City;
use App\Models\Country;
use App\Models\Currency;
use App\Models\Measure;
use App\Models\Product;
use App\Models\Status;
use App\Models\Reserve;
use Auth;
use DB;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Redirect;

class ProductController extends AppBaseController
{
    private $locale;

    public function __construct()
    {
        $this->middleware('auth');
        $this->locale = LaravelLocalization::getCurrentLocale();
    }

    public function index(Request $request)
    {
        $products = Product::whereTranslation('locale', $this->locale)->
        where('user_id', auth()->id())->
        orderBy('status', 'asc')->
        paginate(15);
        return view('dashboard.products.index', compact(['products']));
    }

    public function create()
    {
        if (!Auth::user()->can('product-create')){
            abort(404, 'Unauthorized action.');
        }

        $categories = Category::whereTranslation('locale', $this->locale)->get();
        $categoriesArray = [];
        foreach ($categories as $item) {
            $categoriesArray[$item->id] = $item->title;
        }

        $measures = Measure::whereTranslation('locale', $this->locale)->get();
        $measuresArray = [];
        foreach ($measures as $item) {
            $measuresArray[$item->id] = $item->title;
        }

        $currency = Currency::get();
        $currencyArray = [];
        foreach ($currency as $item) {
            $currencyArray[$item->id] = $item->code;
        }

        $basis = Basis::get();
        $basisArray = [];
        foreach ($basis as $item) {
            $basisArray[$item->id] = $item->code;
        }

        $country = Country::whereTranslation('locale', $this->locale)->get();
        $countryArray = [];
        foreach ($country as $item) {
            $countryArray[$item->id] = $item->title;
        }

        $cities = City::whereTranslation('locale', $this->locale)->get();
        $cityArray = [];
        foreach ($cities as $item) {
            $cityArray[$item->id] = $item->title;
        }

        return view('dashboard.products.create', compact(['categoriesArray', 'measuresArray', 'currencyArray', 'basisArray', 'countryArray', 'cityArray']));
    }

    public function store(Request $request)
    {
        $validation = Product::$rules +
            ['featured_image' => 'required|image|max:2048|mimes:jpeg,png,jpg',
                'name' => 'required|string|min:1|max:255',
                'description' => 'required|string|min:1'];
        $this->validate($request, $validation);

        $helper = new Helper();
        $ostatok = null;
        $saldo = $helper->getUserSaldo($request->currency_id);

        if (!isset($saldo))
            return Redirect::back()->withErrors(['У Вас нету счета на эту валюту']);

        $request->merge([
            'user_id' => auth()->id(),
            'status' => Status::IN_MODERATION_NEW,
            'en' => [
                'title' => $request['name'],
                'description' => $request['description'],
                'conditions' => $request['conditions']
            ],
            'ru' => [
                'title' => $request['name'],
                'description' => $request['description'],
                'conditions' => $request['conditions']
            ],
            'uz' => [
                'title' => $request['name'],
                'description' => $request['description'],
                'conditions' => $request['conditions']
            ]
        ]);

        $product = new Product();
        $product->fill($request->except(['_token', 'name', 'description', 'conditions']));

        if ($request->hasFile('featured_image'))
            $product->featured_image = $this->uploadPhoto($request->file('featured_image'));

        $product->save();

        $product->cities()->sync($request['cities']);

        $message = $helper->reserveMoney($product, Reserve::PRODUCT);
        if ($message) {
            $product->status = Status::NO_PAID_COMMISSION;
            $product->save();
            return redirect(route('ownProducts.index'))->withErrors([$message]);
        }

        return redirect(route('ownProducts.index'))->with('data', 'Product saved successfully.');
    }

    public function edit($id)
    {
        $product = Product::whereTranslation('locale', $this->locale)->
        where('user_id', auth()->id())->
        where('id', $id)->
        first();

        if (empty($product)) {
            return redirect(route('ownProducts.index'))->with('data', 'Product not found');
        }

        $categories = Category::whereTranslation('locale', $this->locale)->get();
        $categoriesArray = [];
        foreach ($categories as $item) {
            $categoriesArray[$item->id] = $item->title;
        }

        $childCategories = ChildCategory::whereTranslation('locale', $this->locale)
            ->where('category_id', $product->parent_category_id)
            ->get();
        $childCategoriesArray = [];
        foreach ($childCategories as $item) {
            $childCategoriesArray[$item->id] = $item->title;
        }

        $measures = Measure::whereTranslation('locale', $this->locale)->get();
        $measuresArray = [];
        foreach ($measures as $item) {
            $measuresArray[$item->id] = $item->title;
        }

        $currency = Currency::get();
        $currencyArray = [];
        foreach ($currency as $item) {
            $currencyArray[$item->id] = $item->code;
        }

        $basis = Basis::get();
        $basisArray = [];
        foreach ($basis as $item) {
            $basisArray[$item->id] = $item->code;
        }

        $country = Country::whereTranslation('locale', $this->locale)->get();
        $countryArray = [];
        foreach ($country as $item) {
            $countryArray[$item->id] = $item->title;
        }

        $cities = City::whereTranslation('locale', $this->locale)->get();
        $cityArray = [];
        foreach ($cities as $item) {
            $cityArray[$item->id] = $item->title;
        }

        return view('dashboard.products.edit', compact(['product', 'categoriesArray', 'measuresArray', 'currencyArray', 'basisArray', 'countryArray', 'cityArray', 'childCategoriesArray']));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, Product::$rules +
            ['name' => 'required|string|min:1|max:255',
                'description' => 'required|string|min:1|max:255']);

        $request->merge([
            'en' => [
                'title' => $request['name'],
                'description' => $request['description'],
                'conditions' => $request['conditions']
            ],
            'ru' => [
                'title' => $request['name'],
                'description' => $request['description'],
                'conditions' => $request['conditions']
            ],
            'uz' => [
                'title' => $request['name'],
                'description' => $request['description'],
                'conditions' => $request['conditions']
            ]
        ]);

        $product = Product::where('user_id', auth()->id())->where('id', $id)->first();

        if (empty($product)) {
            return redirect(route('ownProducts.index'))->
            with('data', 'Product not found');
        }

        $product->fill($request->except(['featured_image']));

        if ($request->hasFile('featured_image')) {
            $this->removePhoto($product->featured_image);
            $product->featured_image = $this->uploadPhoto($request->file('featured_image'));
        }

        $product->status = Status::IN_MODERATION_EDITED;
        $product->save();

        return redirect(route('ownProducts.index'))->with('data', 'Product updated successfully');
    }

    public function destroy($id)
    {
        $product = Product::where('user_id', auth()->id())->
        where('id', $id)->
        first();

        if (empty($product)) {
            return redirect(route('ownProducts.index'))->with('data', 'Product not found');
        }
        $product->status = Status::DEACTIVATED;
        $product->save();

        return redirect(route('ownProducts.index'))->with('data', 'Product deactivated');
    }

    public function refresh($id)
    {
        $product = Product::where('user_id', auth()->id())->where('id', $id)->first();

        if (empty($product))
            return redirect(route('ownProducts.index'))->with('data', 'Product not found');

        $helper = new Helper();
        $ostatok = null;
        $saldo = $helper->getUserSaldo($product->currency_id);

        if (isset($saldo)) {
            $ostatok = round($saldo->saldo, 2, PHP_ROUND_HALF_DOWN);
        } else {
            return Redirect::back()->withErrors(['У Вас нету счета на эту валюту']);
        }

        $message = $helper->reserveMoney($product, Reserve::PRODUCT);
        if ($message) {
            return redirect(route('ownProducts.index'))->withErrors([$message]);
        }

        $product->status = Status::ACTIVE;
        $product->save();
        return redirect(route('ownProducts.index'))->with('data', 'Product фсешмфеув');
    }

    public function getChildCategories(Request $request)
    {
        $categories = ChildCategory::where('category_id', $request->category_id)->
        whereTranslation('locale', $this->locale)->
        get();
        $categoriesArray = [];
        foreach ($categories as $item) {
            $categoriesArray[$item->id] = $item->title;
        }

        return response()->json($categoriesArray);
    }

    protected function uploadPhoto(UploadedFile $file)
    {

        $base_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $image_name = str_slug($base_name, '_') . '_' . time() . '.' . $file->getClientOriginalExtension();

        $file->move(public_path('uploads/product/'), $image_name);

        Image::make(public_path('uploads/product/') . $image_name)
            ->fit(100)
            ->save(public_path('uploads/product/') . 'admin_' . $image_name);

        Image::make(public_path('uploads/product/') . $image_name)
            ->fit(720, 960)
            ->save(public_path('uploads/product/') . 'thumb_' . $image_name);

        Image::make(public_path('uploads/product/') . $image_name)
            ->fit(1200, 1600)
            ->save(public_path('uploads/product/') . 'icon_' . $image_name);

        return $image_name;
    }

    protected function removePhoto($image_name)
    {

        if (!empty($image_name)) {
            unlink(public_path('uploads/product/') . $image_name);
            unlink(public_path('uploads/product/admin_') . $image_name);
            unlink(public_path('uploads/product/thumb_') . $image_name);
            unlink(public_path('uploads/product/icon_') . $image_name);
        }
    }
}
