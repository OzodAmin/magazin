<?php
namespace App\Http\Controllers\Dashboard;

use QrCode;
use App\Models\Contract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\AppBaseController;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ContractController extends AppBaseController
{
    private $locale;

    public function __construct()
    {
        $this->middleware('auth');
        $this->locale = LaravelLocalization::getCurrentLocale();
    }

    public function index(Request $request)
    {
        $contracts = Contract::orWhere('s_client_id', auth()->id())->orWhere('b_client_id', auth()->id())->orderBy('created_at', 'desc')->paginate(15);
        return view('dashboard.contracts.index', compact(['contracts']));
    }

    public function view($id)
    {
        $contract = Contract::where('id', $id)->first();
        return view('dashboard.contracts.show', compact(['contract']));
    }

    public function contractLoad(Request $request)
    {
        $file = resource_path() . '\contracts\ready\_' . $request->doc . '.docx';

        $headers = [
            'Content-Type' => 'application/docx',
        ];

        return response()->download($file, $request->doc . '.docx', $headers);
    }

    public function approve($id)
    {
        $contract = Contract::where('id', $id)->first();

        if (empty($contract)) {
            return redirect(url('contracts'))->with('data', 'Контракт не найден.');
        }

        switch (auth()->id()) {
            case $contract->s_client_id:
                switch ($contract->status) {
                    case 1:
                        $contract->status = 2;
                        break;
                    case 2:
                        return redirect(url('contracts'))->with('data', 'Вы уже одобрили.');
                        break;
                    case 3:
                        $contract->status = 4;
                        $this->contractDocPopulate($contract);
                }
                break;
            case $contract->b_client_id:
                switch ($contract->status) {
                    case 1:
                        $contract->status = 3;
                        break;
                    case 3:
                        return redirect(url('contracts'))->with('data', 'Вы уже одобрили.');
                        break;
                    case 2:
                        $contract->status = 4;
                        $this->contractDocPopulate($contract);
                }
                break;
        }

        $contract->save();
        return redirect(url('contracts'))->with('data', 'Спасибо за одобрение.');
    }

    private function contractDocPopulate(Contract $contract)
    {
        $urlResources = resource_path();
        $url = URL::signedRoute('contractProve', ['id' => $contract->id]);
        $mageUrl = $urlResources . '\contracts\contractsQrCodes\qrcode-' . $contract->docN . '.png';
        $imageWord = array("path" => $mageUrl, "width" => 100, "height" => 100);
        QrCode::format('png')->size(500)->generate($url, $mageUrl);

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($urlResources . '\contracts\tpl\contract.docx');
        $templateProcessor->setValue('contractNumber', $contract->docN);
        $templateProcessor->setValue('sellerBankName', $contract->s_bank_name);
        $templateProcessor->setValue('sellerBankAccount', $contract->s_bank_name);
        $templateProcessor->setValue('sellerBankCode', $contract->s_bank_name);
        $templateProcessor->setValue('buyerBankName', $contract->s_bank_name);
        $templateProcessor->setValue('buyerBankAccount', $contract->s_bank_name);
        $templateProcessor->setValue('buyerrBankCode', $contract->s_bank_name);
        $templateProcessor->setImageValue('image', $imageWord);
        $templateProcessor->saveAs($urlResources . '\contracts\ready\_' . $contract->docN . '.docx');
    }
}