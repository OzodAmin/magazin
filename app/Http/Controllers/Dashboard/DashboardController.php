<?php
namespace App\Http\Controllers\Dashboard;

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\RequestModel;
use App\Models\Reserve;
use App\Models\Product;
use App\Models\Status;
use Carbon\Carbon;
use Entrust;

class DashboardController extends AppBaseController
{
	private $locale;

	public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->locale = LaravelLocalization::getCurrentLocale();
    }

	public function index(Request $request)
    {
        return view('dashboard.index');
    }

    public function notification($id)
    {
        if (Entrust::can('offer-price')) {
            $notification = RequestModel::where('id', $id)->first();

            $products = Product::whereTranslation('locale', $this->locale)->
                                where('user_id', auth()->id())->
                                where('status', Status::ACTIVE)->
                                where('child_category_id', $notification->product->child_category_id)->
                                get();
            $productArray = [];
            foreach($products as $item) {$productArray[$item->id] = $item->title;}

            return view('dashboard.notification', compact(['notification', 'productArray']));
        }
        abort(404, 'Unauthorized action.');
    }

    public function reserves(Request $request)
    {
        $reserves = Reserve::where('IsActive', 1)->where('user_id', auth()->id())->paginate(10);
        return view('dashboard.reserves', compact(['reserves']));
    }
}