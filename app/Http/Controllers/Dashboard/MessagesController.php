<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\DocumentType;
use App\User;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use App\Models\Thread;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MessagesController extends Controller
{
    private $locale;
    const MODERATOR = 1;

    public function __construct()
    {
        $this->middleware('auth');
        $this->locale = LaravelLocalization::getCurrentLocale();
    }

    /**
     * Show all of the message threads to the user.
     *
     * @return mixed
     */
    public function index(Request $request)
    {
        // All threads, ignore deleted/archived participants
        //$threads = Thread::getAllLatest()->get();
        $user = Auth::user();
        $userid = Auth::id();
        if ($user->hasRole('mod')) {
            $userid = self::MODERATOR;
        }

        $threads = Thread::forUser(Auth::id())->latest('updated_at')->where('threads.user_id', '!=', $userid);

        if ($request->has('subject') && $request->subject !== null) {
            $threads->where('subject', $request->subject);
        }
        if ($request->has('doc_number') && $request->doc_number !== null) {
            $threads->where('doc_number', $request->doc_number);
        }
        if ($request->has('doc_date') && $request->doc_date !== null) {
            $threads->where('doc_date', $request->doc_date);
        }
        if ($request->has('status') && $request->status !== null) {
            $threads->where('status', $request->status);
        }
        if ($request->has('sender') && $request->sender !== null) {
            $threads->where('participants.user_id', $request->sender);
        }
        if ($request->has('receipent') && $request->receipent !== null) {
            $threads->where('participants.user_id', $request->receipent);
        }


        $document = DocumentType::whereTranslation('locale', $this->locale)->get();
        $document_type = [];
        foreach ($document as $item) {
            $document_type[$item->id] = $item->name;
        }

        $users = User::all()->where('id', '<>', self::MODERATOR);
        $users_list = [];
        foreach ($users as $item) {
            $users_list[$item->id] = $item->name;
        }

        $threads = $threads->paginate(15);
        // All threads that user is participating in, with new messages
        // $threads = Thread::forUserWithNewMessages(Auth::id())->latest('updated_at')->get();

        return view('dashboard.messenger.index', compact(['threads', 'document_type', 'request', 'users_list']));
    }

    public function sent(Request $request)
    {
        $user = Auth::user();
        $userid = Auth::id();
        if ($user->hasRole('mod')) {
            $userid = self::MODERATOR;
        }

        $threads = Thread::forUser(Auth::id())->latest('updated_at')->where('threads.user_id', '=', $userid);


        if ($request->has('subject') && $request->subject !== null) {
            $threads->where('subject', $request->subject);
        }
        if ($request->has('doc_number') && $request->doc_number !== null) {
            $threads->where('doc_number', $request->doc_number);
        }
        if ($request->has('doc_date') && $request->doc_date !== null) {
            $threads->where('doc_date', $request->doc_date);
        }
        if ($request->has('status') && $request->status !== null) {
            $threads->where('status', $request->status);
        }
        if ($request->has('sender') && $request->sender !== null) {
            $threads->where('participants.user_id', $request->sender);
        }
        if ($request->has('receipent') && $request->receipent !== null) {
            $threads->where('participants.user_id', $request->receipent);
        }


        $document = DocumentType::whereTranslation('locale', $this->locale)->get();
        $document_type = [];
        foreach ($document as $item) {
            $document_type[$item->id] = $item->name;
        }
        $users = User::all()->where('id', '<>', self::MODERATOR);
        $users_list = [];
        foreach ($users as $item) {
            $users_list[$item->id] = $item->name;
        }


        $threads = $threads->paginate(3);
        return view('dashboard.messenger.index', compact(['threads', 'document_type', 'request', 'users_list']));
    }

    /**
     * Shows a message thread.
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');

            return redirect()->route('messages');
        }

        // show current user in list if not a current participant
        // $users = User::whereNotIn('id', $thread->participantsUserIds())->get();

        // don't show the current user in list
        $userId = Auth::id();
        $users = User::whereNotIn('id', $thread->participantsUserIds($userId))->get();

        $thread->markAsRead($userId);

        return view('dashboard.messenger.show', compact('thread', 'users'));
    }

    /**
     * Creates a new message thread.
     *
     * @return mixed
     */
    public function create()
    {
        $user = Auth::user();
        if ($user->hasRole('mod')) {
            $users_all = User::where('id', '!=', Auth::id())->get();
        } else {
            $users_all = User::where('id', '=', self::MODERATOR)->get();
        }

        $users = [];
        foreach ($users_all as $item) {
            $users[$item->id] = $item->name;
        }

        $document = DocumentType::whereTranslation('locale', $this->locale)->get();
        $document_type = [];
        foreach ($document as $item) {
            $document_type[$item->id] = $item->name;
        }

        return view('dashboard.messenger.create', compact(['users', 'document_type']));
    }

    /**
     * Stores a new message thread.
     *
     * @return mixed
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $userid = Auth::id();
        if ($user->hasRole('mod')) {
            $userid = self::MODERATOR;
        }
        $validation = ['attachment' => 'max:4096|mimes:jpeg,png,jpg,pdf'];
        $this->validate($request, $validation);
        $input = Input::all();

        $thread = Thread::create([
            'subject' => $input['subject']
        ]);
        $thread->doc_number = $request->doc_number;
        $thread->doc_date = $request->doc_date;
        $thread->status = 1;
        $thread->user_id = $userid;
        $thread->save();

        // Message
        $message = Message::create([
            'attachment' => $request->file('attachment'),
            'thread_id' => $thread->id,
            'user_id' => $userid,
            'body' => $input['message'],
        ]);

        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => $userid,
            'last_read' => new Carbon,
        ]);

        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipant($input['recipients']);
        }

        if (Input::has('attachment')) {
            $message->attachment = $this->uploadAttachments($request->file('attachment'));
            $message->save();
        }

        return redirect()->route('messages');
    }

    /**
     * Adds a new message to a current thread.
     *
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');

            return redirect()->route('messages');
        }

        $thread->activateAllParticipants();

        $user = Auth::user();
        $userid = Auth::id();
        if ($user->hasRole('mod')) {
            $userid = self::MODERATOR;
        }

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => $userid,
            'body' => Input::get('message'),
        ]);

        // Add replier as a participant
        $participant = Participant::firstOrCreate([
            'thread_id' => $thread->id,
            'user_id' => $userid,
        ]);
        $participant->last_read = new Carbon;
        $participant->save();

        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipant(Input::get('recipients'));
        }

        return redirect()->route('messages.show', $id);
    }

    protected function uploadAttachments(UploadedFile $file)
    {

        $base_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $file_name = str_slug($base_name, '_') . '_' . time() . '.' . $file->getClientOriginalExtension();

        $file->move(public_path('uploads/chat_attachments/'), $file_name);

        return $file_name;
    }
}
