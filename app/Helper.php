<?php

namespace App;

use App\Models\Contract;
use App\Models\Holidays;
use App\Models\Product;
use App\Models\Reserve;
use App\Models\Rkp;
use App\Models\Status;

use App\Models\SystemParams;
use Carbon\Carbon;
use Entrust;


class Helper
{
    public function getUserSaldo(int $currencyId)
    {
        return Rkp::where('user_id', auth()->id())->where('currency_id', $currencyId)->where('status_id', Status::ACTIVE)->first();
    }

    public function reserveMoney(Product $product, int $reserveType)
    {
        $saldo = $this->getUserSaldo($product->currency_id);

        $ostatok = null;
        if (isset($saldo)) {
            $ostatok = round($saldo->saldo, 2, PHP_ROUND_HALF_DOWN);
        } else {
            return 'У Вас нету счета на эту валюту';
        }

        $summa = ($product->quantity * $product->price) / 100;

        switch ($reserveType){
            case Reserve::PRODUCT:
                $prosent = $product->deposit;
                $zalog = $summa * $prosent;
                $zalog = round($zalog, 2, PHP_ROUND_HALF_UP);
                break;
            case Reserve::COMISSION:
                $system = new SystemParams();
                $prosent = $system->getComission();
                $zalog = $summa * $prosent;
                $zalog = round($zalog, 2, PHP_ROUND_HALF_UP);
                break;
            case Reserve::MEHANIZM:
                $prosent = $product->deposit;
                $zalog = $summa * $prosent;
                $zalog = round($zalog, 2, PHP_ROUND_HALF_UP);
                break;
                break;
        }

        if ($zalog > $ostatok) {
            return 'Не достаточно средств на счету';
        }

        $balans = $ostatok - $zalog;
        $balans = round($balans, 2, PHP_ROUND_HALF_DOWN);
        $saldo->saldo = $balans;
        $saldo->save();

        $reserveZalog = new Reserve();
        $reserveZalog->type_id = $reserveType;
        $reserveZalog->user_id = auth()->id();
        $reserveZalog->product_id = $product->id;
        $reserveZalog->currency_id = $product->currency_id;
        $reserveZalog->total = $product->quantity * $product->price;
        $reserveZalog->prosent = $prosent;
        $reserveZalog->IsActive = 1;
        $reserveZalog->summa = $zalog;
        $reserveZalog->save();
    }

    public function workingDays($days)
    {
        $holidays = Holidays::where('holiday', '>', Carbon::now())->get();
        $holidays = $holidays->toArray();

        $MyDateCarbon = Carbon::now();
        $MyDateCarbon->addWeekdays($days);

        for ($i = 1; $i <= $days; $i++) {
            if (in_array(Carbon::now()->addWeekdays($i)->toDateString(), $holidays)) {
                $MyDateCarbon->addDay();
            }
        }
        return $MyDateCarbon;
    }
}
