<?php

namespace App\Providers;

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Models\RequestUser;
use Carbon\Carbon;
use Auth;



class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Schema::defaultStringLength(191);

        view()->composer('*', function ($view) {
            $locale = LaravelLocalization::getCurrentLocale();
            $view->with(compact(['locale']));
        });

        view()->composer('layouts.user', function ($view) {
            $notifications = null;
            if (Auth::check()) {
                $notificationsDB = RequestUser::where('user_id', auth()->id())->get();

                foreach ($notificationsDB as $notification) {
                    if($notification->request->status == 1 && $notification->product_price == null)
                        $notifications = collect([$notification]);                    
                }
            }

            $view->with(compact(['notifications']));
        });
    }

    public function register()
    {
        //
    }
}
