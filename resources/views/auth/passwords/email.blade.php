@extends('layouts.app')

@section('title')
    Email
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xl-5 offset-xl-3">

                <div class="login-register-page">
                    <!-- Welcome Text -->
                    <div class="welcome-text">
                        <h3>Write your valid email</h3>
                        <span>Don't have an account? <a href="{{ url('register') }}">Sign Up!</a></span>
                    </div>

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                @endif

                    <form role="form" method="POST" action="{{ route('password.email') }}" class="login-form">
                        @csrf
                        <div class="input-with-icon-left">
                            <i class="icon-feather-user"></i>
                            <input type="email"
                                   name="email"
                                   placeholder="Email"
                                   value="{{ old('email') }}"
                                   class="input-text with-border form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                   id="email" required autofocus>
                        </div>

                        <button class="button full-width button-sliding-icon ripple-effect margin-top-10" type="submit">Send <i class="icon-material-outline-arrow-right-alt"></i></button>
                    </form>

                </div>

            </div>
        </div>
    </div>
    <div class="margin-top-70"></div>
@endsection