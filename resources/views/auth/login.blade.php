@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-5 offset-xl-3">


            <div class="login-register-page">
                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>We're glad to see you again!</h3>
                    <span>Don't have an account? <a href="{{ url('register') }}">Sign Up!</a></span>
                </div>
                    
                <!-- Form -->
                <form role="form" method="POST" action="{{ route('login') }}" id="login">
                    @csrf
                    <div class="input-with-icon-left">
                        <i class="icon-feather-user"></i>
                        <input
                                type="text"
                                class="input-text with-border"
                                name="username"
                                id="emailaddress"
                                value="{{ old('username') }}"
                                placeholder="Username"
                                required/>
                    </div>

                    <div class="input-with-icon-left">
                        <i class="icon-material-outline-lock"></i>
                        <input 
                                type="password" 
                                class="input-text with-border" 
                                name="password" 
                                id="password"
                                placeholder="Password" required/>
                    </div>
                    <a href="{!! route('password.request') !!}" class="forgot-password">Forgot Password?</a>
                    <!-- Button -->
                    <button class="button full-width button-sliding-icon ripple-effect margin-top-10" type="submit" form="login">Log In <i class="icon-material-outline-arrow-right-alt"></i></button>
                </form>
                
            </div>

        </div>
    </div>
</div>
<div class="margin-top-70"></div>
@endsection