@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xl-5 offset-xl-3">

                <div class="login-register-page">
                    <!-- Welcome Text -->
                    <div class="welcome-text">
                        <h3 style="font-size: 26px;">Let's create your account!</h3>
                        <span>Already have an account? <a href="{{ url('login') }}">Log In!</a></span>
                    </div>
                    <form role="form" method="POST" action="{{ url('registeration') }}" id="register-account-form">
                    {{ csrf_field() }}
                    <!-- Account Type -->
                        <div class="account-type">
                            <div>
                                <input type="radio" name="role" id="freelancer-radio" value="8"
                                       class="account-type-radio" checked/>
                                <label for="freelancer-radio" class="ripple-effect-dark"><i
                                            class="icon-material-outline-account-circle"></i> Участник</label>
                            </div>

                            <div>
                                <input type="radio" name="role" id="employer-radio" value="9"
                                       class="account-type-radio"/>
                                <label for="employer-radio" class="ripple-effect-dark"><i
                                            class="icon-material-outline-business-center"></i> Заказчик</label>
                            </div>
                        </div>

                        <div class="input-with-icon-left" title="Наименование организации"
                             data-tippy-placement="bottom">
                            <i class="icon-material-outline-account-balance"></i>
                            <input type="text" value="{{ old('company_legal_name') }}" class="input-text with-border"
                                   name="company_legal_name" placeholder="Наименование организации" required/>
                        </div>

                        <div class="input-with-icon-left" title="Юридический адрес" data-tippy-placement="bottom">
                            <i class="icon-material-outline-add-location"></i>
                            <input type="text" value="{{ old('address') }}" class="input-text with-border"
                                   name="address" placeholder="Юридический адрес" required/>
                        </div>

                        <div class="input-with-icon-left" title="ИНН должно быт шестизначное число"
                             data-tippy-placement="bottom">
                            <i class="icon-material-outline-assignment"></i>
                            <input type="text" value="{{ old('inn') }}" class="input-text with-border" name="inn"
                                   placeholder="ИНН" onkeypress="javascript:return isNumber(event)" required/>
                        </div>

                        <div class="input-with-icon-left" title="Фамилия Имя Отчество" data-tippy-placement="bottom">
                            <i class="icon-feather-user"></i>
                            <input type="text" value="{{ old('name') }}" class="input-text with-border" name="name"
                                   placeholder="Ф.И.О" required/>
                        </div>

                        <div class="input-with-icon-left" title="Aдрес электронной почты" data-tippy-placement="bottom">
                            <i class="icon-material-baseline-mail-outline"></i>
                            <input type="email" value="{{ old('email') }}" class="input-text with-border" name="email"
                                   id="emailaddress-register" placeholder="Email Address" required/>
                        </div>

                        <div class="input-with-icon-left" title="Контактный номер (xx) xxx-xxxx"
                             data-tippy-placement="bottom">
                            <i class="icon-line-awesome-phone"></i>
                            <input type="tel" value="{{ old('phone') }}" class="input-text with-border" id="phone"
                                   name="phone" placeholder="Телефон" required/>
                        </div>
                        <!-- Button -->
                        <button class="margin-top-10 button full-width button-sliding-icon ripple-effect" type="submit"
                                form="register-account-form">Register <i
                                    class="icon-material-outline-arrow-right-alt"></i>
                        </button>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <div class="margin-top-70"></div>
@endsection