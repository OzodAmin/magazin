@extends('layouts.app')

@section('title')
    Products list
@endsection

@section('content')
<!-- Page Content ==================== -->
<div class="full-page-container">
    <div class="full-page-content-container" data-simplebar>
        <div class="full-page-content-inner">

            <br>
            {!! Form::open(['method'=>'GET','url'=>Request::fullUrl(),'class'=>'navbar-form navbar-left','role'=>'search', 'autocomplete' => 'off'])  !!}

            <div class="input-group custom-search-form">

            </div>
            <?php //var_dump($document_type)?>

            <input type="text" class="form-control" name="produced_year" placeholder="Год производства" value=<?=$request->produced_year?>>
            <input type="text" class="form-control" name="lot_number" placeholder="№ Лота" value=<?=$request->lot_number?>>
            <div class="form-group">
                {!! Form::select('manufacturer_country_id', $countryArray, $request->manufacturer_country_id,
                ['placeholder' => 'Страна производства']); !!}

            </div>
            <input type="text" class="form-control" name="manufacturer_title" placeholder="Производитель" value=<?=$request->manufacturer_title?>>
            <div class="form-group">
                {!! Form::select('measure_id', $measuresArray, $request->measure_id,
                ['placeholder' => 'Единица измерения']); !!}

            </div>

            <input type="text" class="form-control" name="quantity_min" placeholder="Количество от" value=<?=$request->quantity_min?>>
            <input type="text" class="form-control" name="quantity_max" placeholder="Количество до" value=<?=$request->quantity_max?>>

            {{--@if(Auth::user()->hasRole('mod'))--}}

            {{--@if($route == 'messages')--}}
            {{--<div class="form-group">--}}
            {{--{!! Form::select('sender', $users_list, $request->sender,--}}
            {{--[ 'placeholder' => 'Отправитель']); !!}--}}
            {{--</div>--}}
            {{--@else--}}
            {{--<div class="form-group">--}}
            {{--{!! Form::select('receipent', $users_list, $request->receipent,--}}
            {{--[ 'placeholder' => 'Получатель']); !!}--}}
            {{--</div><br>--}}
            {{--@endif--}}

            {{--@endif--}}


            <span class="btn-primary">
        <button class="btn btn-default" type="submit">
            <i class="fa fa-search">Поиск</i>
        </button>
        <button class="btn btn-default">
            <a href="">
                        Сбросить
                    </a>
        </button>
        </span>
        {!! Form::close() !!}

            <!-- Freelancers List Container -->
            <div class="freelancers-container freelancers-grid-layout">
                @foreach ($products as $product)
                <!--Freelancer -->
                <div class="freelancer">
                    <!-- Overview -->
                    <div class="freelancer-overview">
                        <div class="freelancer-overview-inner">
                            <!-- Avatar -->
                            <div class="">
                                <a href="{{ LaravelLocalization::getLocalizedURL($locale, 'product/'.$product->translate($locale)->slug) }}">
                                    <img src="/uploads/product/admin_{{$product->featured_image}}" alt="" class="img-responsive img-thumbnail">
                                </a>
                            </div>

                            <!-- Name -->
                            <div class="freelancer-name">
                                <h4>
                                    <a href="{{ LaravelLocalization::getLocalizedURL($locale, 'product/'.$product->translate($locale)->slug) }}">{{ $product->title }}</a>
                                </h4>
                                <span>
                                    <strong>Цена</strong> 
                                    <?= number_format($product->price,2); ?> {{ $product->currency->code }}
                                </span>
                                <br>
                                <span>
                                    <strong>Кол-во</strong>
                                    {{ $product->quantity }} {{ $product->measureTable->title_short }}
                                </span>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Details -->
                    <div class="freelancer-details">
                        <div data-countdown="<?= $product->expire_at; ?>"></div>
                        <div style="display: none;">{{$product->id}}</div>
                        <div class="freelancer-details-list">
                            <ul>
                                <li>День<strong id="days{{$product->id}}"></strong></li>
                                <li>Час<strong id="hours{{$product->id}}"></strong></li>
                                <li>Минут<strong id="minutes{{$product->id}}"></strong></li>
                                <li>Сек<strong id="seconds{{$product->id}}"></strong></li>
                            </ul>
                        </div>
                        <a href="{{ LaravelLocalization::getLocalizedURL($locale, 'product/'.$product->translate($locale)->slug) }}" class="button button-sliding-icon ripple-effect">
                            Просмотр <i class="icon-material-outline-arrow-right-alt"></i>
                        </a>
                    </div>
                </div>
                <!-- Freelancer / End -->
                @endforeach
            </div>
            <!-- Freelancers Container / End -->

            <!-- Pagination -->
            {{ $products->links() }}
            <!-- Pagination / End -->
        </div>
    </div>
    <!-- Full Page Content / End -->
</div>
@endsection

@section('scripts')
<script src="{{ asset('front/js/jquery.countdown.min.js') }}"></script>
<script>
    $('[data-countdown]').each(function() {
        var $this = $(this), finalDate = $(this).data('countdown');
        $this.countdown(finalDate, function(event) {
            var id = $this.next().text();
            document.getElementById("days"+id).innerHTML = event.strftime('%D');
            document.getElementById("hours"+id).innerHTML = event.strftime('%H');
            document.getElementById("minutes"+id).innerHTML = event.strftime('%M');
            document.getElementById("seconds"+id).innerHTML = event.strftime('%S');
        });
    });
</script>


<script src="{{ asset('datePicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('datePicker/locales/bootstrap-datepicker.ru.min.js') }}"></script>

<script>

    $('#datePicker').datepicker({
        format: "yyyy",
        weekStart: 1,
        clearBtn: true,
        autoclose: true,
        language: "{{ str_replace('_', '-', app()->getLocale()) }}"
    });

    $('#datePickerYear').datepicker({
        format: "yyyy",
        viewMode: "years",
        autoclose: true,
        minViewMode: "years"
    });


</script>
@endsection
