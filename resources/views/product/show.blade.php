@extends('layouts.app')

@section('title')
    {{ $product->title }}
@endsection

@section('content')
<!-- Titlebar
================================================== -->
<div class="single-page-header" data-background-image="{{ asset('front/images/single-job.jpg') }}">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="single-page-header-inner">
                    <div class="left-side">
                        <div class="header-image">
                            {{ Html::image('uploads/product/admin_'.$product->featured_image) }}
                        </div>
                        <div class="header-details">
                            <h3>{{ $product->title }}</h3>
                        </div>
                    </div>
                    <div class="right-side">
                        <div class="salary-box">
                            <div class="salary-amount">
                                <?= number_format($product->price,2); ?> {{ $product->currency->code }}/{{ $product->measureTable->title_short }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Page Content ================================== -->
<div class="container">
    <div class="row">        
        <!-- Content -->
        <div class="col-xl-8 col-lg-8 content-right-offset">
            <div class="single-page-section">
                <h3>Регионы доставки</h3>
                <p>
                    @foreach($product->cities as $city)
                        <mark class="color">{{ $city->title }}</mark>
                    @endforeach
                </p>
                <h3 class="margin-bottom-25">Product Description</h3>
                <p>{{ $product->description }}</p>

                <h3 class="margin-bottom-25">Product Requirements</h3>
                <p>{{ $product->conditions }}</p>
            </div>
        </div>
        

        <!-- Sidebar -->
        <div class="col-xl-4 col-lg-4">
            <div class="sidebar-container">
                @permission('price-process')
                    @if(auth()->id() != $product->user_id)
                        <a href="#small-dialog" class="button full-width button-sliding-icon apply-now-button popup-with-zoom-anim ripple-effect">Механизм запрос цен <i class="icon-material-outline-arrow-right-alt"></i></a>
                    @else
                        <h3 class="button full-width">Это Ваш продукт</h3>
                    @endif
                @endpermission
                    
                <!-- Sidebar Widget -->
                <div class="sidebar-widget">
                    <div class="job-overview">
                        <div class="job-overview-inner">
                            <ul>
                                <li>
                                    <i class="icon-material-outline-business"></i>
                                    <span>Производитель</span>
                                    <h5>{{ $product->manufacturer_title }}</h5>
                                </li>
                                <li>
                                    <i class="icon-material-outline-location-on"></i>
                                    <span>Страна производитель</span>
                                    <h5>{{ $product->country->title }}</h5>
                                </li>
                                <li>
                                    <i class="icon-material-outline-add-shopping-cart"></i>
                                    <span>Количество</span>
                                    <h5>
                                        <b id="max_order">{{ $product->quantity }}</b> {{ $product->measureTable->title_short }}
                                    </h5>
                                </li>
                                <li>
                                    <i class="icon-material-outline-assessment"></i>
                                    <span>Мин/макс партия</span>
                                    <h5>
                                        <b id="min_order">{{ $product->min_order }}</b>
                                        &nbsp;{{ $product->measureTable->title_short }}
                                     </h5>
                                </li>
                                <li>
                                    <i class="icon-material-outline-access-time"></i>
                                    <span>Срок доставки</span>
                                    <h5>{{ $product->basis_day }}</h5>
                                </li>
                                <li>
                                    <i class="icon-material-outline-access-time"></i>
                                    <span>Date Posted</span>
                                    <h5>{{ $product->created_at }}</h5>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@permission('price-process')
<!-- Leave a Review for Freelancer Popup
================================================== -->
<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">
    <!--Tabs -->
    <div class="sign-in-form">
        <div class="popup-tabs-container">
            <div class="popup-tab-content">
                
                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>Механизм запрос цен</h3>
                </div>
                    
                <!-- Form -->
                {!! Form::open(['method' => 'POST','url' => 'price-request', 'onsubmit' => 'return validateForm()']) !!}

                    <input name="productId" type="hidden" value="{{ $product->id }}">

                    <strong>Регион доставки</strong>
                    <select class="selectpicker with-border" data-live-search="true" required title='-- SELECT --' name='cityId'>
                        @foreach($product->cities as $city)
                            <option value="{{ $city->id }}">{{ $city->title }}</option>
                        @endforeach
                    </select>

                    <strong>Требуемое кол-во</strong>
                    <input class="with-border" id="quantity" onkeypress="javascript:return isNumber(event)" placeholder="Требуемое кол-во" name="owner_product_quantity" type="text" required>


                    <strong>Оплата в банковских днях</strong>
                    <input class="with-border" onkeypress="javascript:return isNumber(event)" placeholder="Оплата" name="pay_date" type="text" required>

                    <strong>Примечание</strong>
                    <textarea class="with-border" placeholder="Примечание" name="comment" cols="7"></textarea>

                    <button class="button full-width button-sliding-icon ripple-effect" type="submit">
                        Запустит механизм <i class="icon-material-outline-arrow-right-alt"></i>
                    </button>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endpermission
@endsection

@section('scripts')
<script>
    function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
            return false;
        return true;
    }

    const $quantity = document.querySelector('#quantity');
    const $minOrder = parseFloat(document.getElementById('min_order').innerHTML);
    const $maxOrder = parseFloat(document.getElementById('max_order').innerHTML);

    function validateForm() {
        if($maxOrder < parseFloat($quantity.value) || parseFloat($quantity.value) < $minOrder) {
            swal("Ooops..", "Quantity input is wrong", "error");
            return false;
        }
    }
</script>
@endsection