<?php ?>
@extends('admin.layouts.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Контракты
            <div class="panel-body">
                @include('flash::message')


                {!! Form::open(['method'=>'GET','url'=>Request::fullUrl(),'class'=>'navbar-form navbar-left','role'=>'search', 'autocomplete' => 'off'])  !!}

                <table class="table table-striped table-bordered table-condensed">
                    <thead>
                    <th><input type="text"  name="contract_number" placeholder="№" class="form-control" value={{$request->contract_number}}></th>

                    <th>{!! Form::select('seller', $sellerArray, $request->seller,
                ['placeholder' => 'Продавец', 'class' => 'form-control']); !!}</th>
                    <th>{!! Form::select('buyer', $buyerArray, $request->buyer,
                ['placeholder' => 'Покупатель', 'class' => 'form-control']); !!}</th>
                    <th>

                        {{ Form::text('date',
                           $request->date,
                           [ 'id' => 'datePicker', 'placeholder' => 'Дата создания', 'class' => 'form-control' ])
                       }}
                    </th>
                    <th><input type="text"  name="summa" placeholder="Сумма договора" class="form-control" value={{$request->summa}}></th>

                    <th><button class="btn btn-default" type="submit">
                            <i class="fa fa-search">Поиск</i>
                        </button>
                        <button class="btn btn-default">
                            <a href="/backend/contracts">
                                Сбросить
                            </a>
                        </button></th>

                    </tr>
                    </thead>
                </table>

                {!! Form::close() !!}

                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Продавец</th>
                        <th>Покупатель</th>
                        <th>Дата</th>
                        <th>Сумма/начислено(%)</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($contracts as $contract)
                        <tr class="">
                            <td class="col-sm-2">
                                {{ $contract->docN }}
                            </td>
                            <td class="col-sm-3">
                                {{ $contract->seller->company_legal_name }}

                                {!! Form::open(['route' =>'docPayment.create']) !!}

                                <input type="hidden" name="contractId" value="<?= $contract->id; ?>">
                                <input type="hidden" name="currencyId" value="<?= $contract->currency_id; ?>">
                                <input type="hidden" name="userId" value="<?= $contract->seller->id; ?>">
                                <button type="submit" class="btn btn-success"><i class="fa fa-btn fa-upload"></i></button>

                                {!! Form::close() !!}
                            </td>
                            <td class="col-sm-3">
                                {{ $contract->buyer->company_legal_name }}
                            </td>
                            <td class="col-sm-2">
                                {{ $contract->created_at }}
                            </td>
                            <td class="col-sm-2">
                                {{ $contract->summa.' '.$contract->currency->code }}
                                <p class="bg-info">...</p>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{ $contracts->links() }}
@endsection

@section('scripts')
    <script src="{{ asset('datePicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('datePicker/locales/bootstrap-datepicker.ru.min.js') }}"></script>

    <script>

        $('#datePicker').datepicker({
            format: "yyyy-mm-dd",
            weekStart: 1,
            clearBtn: true,
            autoclose: true,
            language: "{{ str_replace('_', '-', app()->getLocale()) }}"
        });

    </script>
@endsection
