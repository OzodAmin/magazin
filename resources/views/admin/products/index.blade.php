<?php ?>
@extends('admin.layouts.app')

@section('content')
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                Product Management
            </div>
            <div class="panel-body">
                @include('flash::message')

                {!! Form::open(['method'=>'GET','url'=>Request::fullUrl(),'class'=>'navbar-form navbar-left','role'=>'search', 'autocomplete' => 'off'])  !!}

                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        {{--<th>Image</th>--}}
                        <th><input type="text" name="name" placeholder="Наименование" class="form-control"
                                   value={{$request->name}}></th>

                        <th>
                            <div class="form-group">
                                {!! Form::select('status',$statusesArray, $request->status, ['placeholder' => 'Статус', 'class' => 'form-control']) !!}

                            </div>
                        </th>
                        <th>
                            <div class="form-group">
                                {!! Form::select('main_category',
                        $categoriesArray,
                        $request->main_category,
                        [
                        'placeholder' => 'Основная категория',
                            'class' => 'form-control',
                            'id' => 'categoryId',
                            'data-live-search' => 'true',
                            'data-size' => '7',
                            'title' => '-- SELECT --',
                        ]) !!}

                            </div>
                        </th>


                        <th>
                            {!! Form::select('child_category',
                        $categoriesArray,
                        $request->child_category,
                        [
                        'placeholder' => 'Дочерняя категория',
                        'id' => 'childCategoryId',
                            'class' => 'form-control',
                            'data-live-search' => 'true',
                            'data-size' => '7',
                            'title' => '-- SELECT --',
                        ]) !!}
                        </th>
                        <th>
                            <button class="btn btn-default" type="submit">
                                <i class="fa fa-search">Поиск</i>
                            </button>
                            <button class="btn btn-default">
                                <a href="/backend/products">
                                    Сбросить
                                </a>
                            </button>
                        </th>
                        <th></th>
                    </tr>
                    </thead>
                </table>

                {!! Form::close() !!}
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Main Category</th>
                        <th>Child Category</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($products as $product)

                        <tr class="products-users">
                            <td class="col-sm-2">
                                <center>
                                    {{ Html::image('uploads/product/admin_'.$product->featured_image) }}
                                </center>
                            </td>
                            <td class="col-sm-2">
                                {{ $product->title }}
                            </td>
                            <td class="col-sm-2">
                                        <span class="label {{ $product->statusTable->admin_css }}">
                                            {{ $product->statusTable->name }}
                                        </span>
                            </td>
                            <td class="col-sm-2">
                                {{ $product->categoryMain->title }}
                            </td>
                            <td class="col-sm-2">
                                {{ $product->categoryChild->title }}
                            </td>
                            <td class="col-sm-2">
                                <a class="btn btn-primary" href="{{ route('products.edit',$product->id) }}"><i
                                            class="fa fa-btn fa-edit"></i> Edit</a>

                                <form action="{{ url('backend/products/'.$product->id) }}" method="POST"
                                      style="display: inline-block">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <button type="submit" id="delete-task-{{ $product->id }}" class="btn btn-danger">
                                        <i class="fa fa-btn fa-trash"></i> Delete
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{ $products->links() }}
    </div>
@endsection

@section('scripts')
    <script>
        $('#categoryId').on('change', function () {
            var categoryId = $(this).val();
            if (categoryId) {
                var select = document.getElementById("childCategoryId");
                var length = select.options.length;
                for (var i = 0; i < length; i++) {
                    select.options[i] = null;
                }
                $.ajax({
                    type: "GET",
                    url: "{{url('api/getChildCategories')}}?category_id=" + categoryId,
                    success: function (res) {
                        $("#childCategoryId").append('<option selected="selected" value="">Дочерняя категория</option>');
                        $.each(res, function (key, value) {
                            console.log(value);
                            $("#childCategoryId").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                });
            }
        });
    </script>
@endsection
