<?php ?>
@extends('admin.layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">User Info</div>

                <div class="panel-body">
                    <div class="form-group">
                        <label for="name" class="col-md-4 control-label">Ф.И.О</label>
                        {{ $user->name }}
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <label for="name" class="col-md-4 control-label">E-mail</label>
                        {{ $user->email }}
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <label for="name" class="col-md-4 control-label">Название заведения</label>
                        {{ $user->company_legal_name }}
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <label for="name" class="col-md-4 control-label">Инн</label>
                        {{ $user->inn }}
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <label for="name" class="col-md-4 control-label">Адрес</label>
                        {{ $user->address }}
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <label for="name" class="col-md-4 control-label">Контактный номер</label>
                        {{ $user->phone }}
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <label for="name" class="col-md-4 control-label">Роль</label>
                        @if(!empty($user->roles))
                            @foreach($user->roles as $role)
                                <label class="label label-success">{{ $role->display_name }}</label>
                            @endforeach
                        @endif
                    </div>
                    <div class="clearfix"></div>
                    <a href="{{ route('users.index') }}" class="btn btn-default">Назад в список</a>
                </div>
            </div>
        </div>
    </div>
@endsection