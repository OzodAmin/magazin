<?php ?>
@extends('admin.layouts.app')

@section('content')
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Пользователи
                </div>

                <div class="panel-body">
                    @include('flash::message')
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    {!! Form::open(['method'=>'GET','url'=>Request::fullUrl(),'class'=>'navbar-form navbar-left','role'=>'search', 'autocomplete' => 'off'])  !!}

                    <table class="table table-striped table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th><input type="text" name="name" placeholder="ФИО" class="form-control"
                                       value={{$request->name}}></th>
                            <th><input type="text" name="email" placeholder="E-mail" class="form-control"
                                       value={{$request->email}}></th>
                            <th><input type="text" name="company" placeholder="Организация" class="form-control"
                                       value={{$request->company}}></th>
                            <th>{!! Form::select('role', $rolesArray, $request->role,
                ['placeholder' => 'Роль','class' => 'form-control']); !!}</th>
                            <th>{!! Form::select('status', $statusesArray, $request->status,
                ['placeholder' => 'Статус', 'class' => 'form-control']); !!}</th>
                            <th></th>
                        </tr>
                        <th>
                            <button class="btn btn-default" type="submit">
                                <i class="fa fa-search">Поиск</i>
                            </button>
                            <button class="btn btn-default">
                                <a href="/backend/users">
                                    Сбросить
                                </a>
                            </button>
                        </th>

                        </thead>
                    </table>

                    {!! Form::close() !!}

                    <table class="table table-striped table-bordered table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>Ф.И.О</th>
                            <th>Email</th>
                            <th>Логин</th>
                            <th>Организация</th>
                            <th>Роль</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($users as $user)

                            <tr class="list-users">
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->username }}</td>
                                <td>{{ $user->company_legal_name }}</td>
                                <td>
                                    @foreach($user->roles as $role)
                                        {{ $role->display_name }}
                                    @endforeach
                                </td>
                                <td>
                                    @switch($user->status)
                                        @case(1)
                                        <span class="label label-default statuses">
                                                    На модерации
                                                </span>
                                        @break
                                        @case(2)
                                        <span class="label label-success">
                                                    Активный
                                                </span>
                                        @break
                                        @case(3)
                                        <span class="label label-danger">
                                                    Нарушение правил торгов
                                                </span>
                                        @break
                                        @case(4)
                                        <span class="label label-danger">
                                                    Неуплата комиссионных сборов
                                                </span>
                                        @break
                                        @case(5)
                                        <span class="label label-danger">
                                                    Неполнота предоставленных документов
                                                </span>
                                        @break
                                        @case(6)
                                        <span class="label label-danger">
                                                    Неактивен по прочим причинам
                                                </span>
                                        @break
                                        @case(7)
                                        <span class="label label-warning">
                                                    Заблокирован сотрудником РКП
                                                </span>
                                        @break
                                        @default
                                        <span class="label label-primary">???</span>
                                    @endswitch

                                </td>
                                <td>
                                    <a class="btn btn-info" href="{{ route('users.show',$user->id) }}">
                                        <i class="fa fa-btn fa-eye"></i>&nbsp;Show
                                    </a>
                                    <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">
                                        <i class="fa fa-btn fa-edit"></i>&nbsp;Edit
                                    </a>

                                    <form action="{{ url('backend/users/'.$user->id) }}" method="POST"
                                          style="display: inline-block">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}

                                        <button type="submit" id="delete-task-{{ $user->id }}" class="btn btn-danger">
                                            Заблокировать
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            {{ $users->links() }}
        </div>
@endsection
