<?php ?>
@extends('admin.layouts.app')

@section('content')
    <section class="content-header">
        <h1>Настройка системы</h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::model($params, ['route' => ['params.update'], 'method' => 'put']) !!}

                    <div class="nav-tabs-custom col-sm-12">
                        <div class="form-group col-sm-6">
                            {!! Form::label('zadatok_seller', 'Задаток продавца') !!}
                            {{ Form::text('zadatok_seller', $params->zadatok_seller, ['class' => 'form-control', 'required' => 'required','onkeypress' => 'javascript:return isNumber(event)']) }}
                        </div>
                        <div class="form-group col-sm-6">
                            {!! Form::label('zadatok_buyer', 'Задаток покупателя') !!}
                            {{ Form::text('zadatok_buyer', $params->zadatok_buyer, ['class' => 'form-control', 'required' => 'required']) }}
                        </div>

                        <div class="clearfix"></div>

                        <div class="form-group col-sm-6">
                            {!! Form::label('comission_seller', 'Комиссия продавца') !!}
                            {{ Form::text('comission_seller', $params->comission_seller, ['class' => 'form-control', 'required' => 'required']) }}
                        </div>
                        <div class="form-group col-sm-6">
                            {!! Form::label('comission_buyer', 'Комиссия покупателя') !!}
                            {{ Form::text('comission_buyer', $params->comission_buyer, ['class' => 'form-control', 'required' => 'required']) }}
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-sm-12">
                        {!! Form::submit('Изменить', ['class' => 'btn btn-primary']) !!}
                        <a href="{!! url('backend') !!}" class="btn btn-default">Cancel</a>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        function isNumber(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        }
    </script>
@endsection