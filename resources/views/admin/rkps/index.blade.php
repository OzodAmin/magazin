<?php ?>
@extends('admin.layouts.app')

@section('content')
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                Rkp Management
            </div>

            <div class="panel-body">
                @include('flash::message')

                {!! Form::open(['method'=>'GET','url'=>Request::fullUrl(),'class'=>'navbar-form navbar-left','role'=>'search', 'autocomplete' => 'off'])  !!}

                <table class="table table-striped table-bordered table-condensed">
                    <thead>

                    <th>{!! Form::select('status', $statusArray, $request->status,
                ['placeholder' => 'Статус', 'class' => 'form-control']); !!}</th>
                    <th></th>
                    <th><input type="text" name="bank_name" placeholder="Наименование банка" class="form-control"
                               value={{$request->bank_name}}></th>

                    <th>{!! Form::select('company', $userArray, $request->company,
                ['placeholder' => 'Клиент', 'class' => 'form-control']); !!}</th>
                    <th>{!! Form::select('currency', $currencyArray, $request->currency,
                ['placeholder' => 'Валюта','class' => 'form-control']); !!}</th>
                    <th><input type="text" name="saldo" placeholder="Остаток" class="form-control"
                               value={{$request->saldo}}></th>
                    <th></th>
                    </tr>
                    <th>
                        <button class="btn btn-default" type="submit">
                            <i class="fa fa-search">Поиск</i>
                        </button>
                        <button class="btn btn-default">
                            <a href="/backend/rkpsAdmin">
                                Сбросить
                            </a>
                        </button>
                    </th>

                    </thead>
                </table>

                {!! Form::close() !!}

                <table class="table table-striped table-bordered table-condensed">
                    <thead>
                    <tr>
                        <th>Status</th>
                        <th>Наименование банка</th>
                        <th>Клиент</th>
                        <th>Валюта</th>
                        <th>Остаток</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($rkps as $item)

                        <tr class="">
                            <td>
                                        <span class="label {{ $item->statusTable->admin_css }}">
                                            {{ $item->statusTable->name }}
                                        </span>
                            </td>
                            <td>{{ $item->bank_name }}</td>
                            <td>{{ $item->user->company_legal_name }}</td>
                            <td>{{ $item->currency->title }}</td>
                            <td>{{ $item->saldo }}</td>

                            <td>
                                @if($item->status_id == Status::ACTIVE)
                                    <a class="btn btn-success"
                                       href="{{ url('backend/payments/create?id='.$item->id) }}">
                                        <i class="fa fa-btn fa-upload"></i>
                                    </a>
                                @endif
                                <a class="btn btn-success" href="{{ route('rkpsAdmin.edit',$item->id) }}">
                                    <i class="fa fa-btn fa-edit"></i>&nbsp;Edit
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{ $rkps->links() }}
    </div>
@endsection
