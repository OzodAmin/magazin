@extends('layouts.app')

@section('content')
<div class="container">
    <div id="app" class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    @guest
                    Not authorized
                    @else
                    {{ Auth::user()->name }}
                    @endguest
                </div>
            </div>
        </div>
    </div>

</div>


@endsection
