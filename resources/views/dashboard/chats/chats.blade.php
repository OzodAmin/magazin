@extends('dashboard.layouts.app')

@section('content')

    <div class="container">
        <div id="app">

        <chat-log :messages="messages"></chat-log>
        <chat-composer v-on:messagesent="addMessage"></chat-composer>

        </div>

        <script src="/js/app.js" charset="utf-8"></script>

    </div>

@endsection
