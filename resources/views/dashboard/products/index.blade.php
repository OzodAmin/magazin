@extends('dashboard.layouts.app')

@section('title')
    Own products
@endsection
@section('content')

    @if (count($errors) > 0)
        <div class="notification error closeable">
            @foreach ($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
            <a class="close" href="#"></a>
        </div>
    @endif

    <div class="dashboard-headline">
        <h3>Мои товары</h3>

        <!-- Breadcrumbs -->
        <nav id="breadcrumbs" class="dark">
            <ul>
                <li>
                    <a href="{{ route('ownProducts.create') }}">
                        Добавить товар&nbsp;<i class="icon-feather-file-plus"></i>
                    </a>
                </li>
            </ul>
        </nav>
    </div>

    <!-- Row -->
    <div class="row">

        <!-- Dashboard Box -->
        <div class="col-xl-12">
            <div class="dashboard-box margin-top-0">
                <div class="content">
                    <ul class="dashboard-box-list">
                        @foreach ($products as $product)
                            <li>
                                <!-- Job Listing -->
                                <div class="job-listing">

                                    <!-- Job Listing Details -->
                                    <div class="job-listing-details">

                                        <!-- Logo -->
                                        <a href="{{ LaravelLocalization::getLocalizedURL($locale, 'product/'.$product->slug) }}"
                                           class="job-listing-company-logo">
                                            {{ Html::image('uploads/product/admin_'.$product->featured_image) }}
                                        </a>

                                        <!-- Details -->
                                        <div class="job-listing-description">
                                            <h3 class="job-listing-title">
                                                <a href="{{ LaravelLocalization::getLocalizedURL($locale, 'product/'.$product->slug) }}">
                                                    {{ $product->title }}
                                                </a>
                                            </h3>

                                            <!-- Job Listing Footer -->
                                            <div class="job-listing-footer">
                                                <ul>
                                                    <li>
                                                <span class="dashboard-status-button {{ $product->statusTable->front_css }}">
                                                    {{ $product->statusTable->name }}
                                                </span>
                                                    </li>
                                                    <br>
                                                    <li>
                                                        <i class="icon-material-outline-date-range"></i>
                                                        &nbsp;Posted
                                                        on {{ Carbon\Carbon::parse($product->created_at)->format('d/m/Y') }}
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Buttons -->
                                <div class="buttons-to-right always-visible">
                                    <a href="#" class="button ripple-effect">
                                        {{ $product->categoryMain->title }}
                                        <i class="icon-material-outline-arrow-right"></i>
                                    </a>
                                    <a href="#" class="button ripple-effect">
                                        {{ $product->categoryChild->title }}
                                    </a>
                                    <a href="{{ route('ownProducts.edit',$product->id) }}"
                                       class="button gray ripple-effect ico" title="Изменить" data-tippy-placement="top">
                                        <i class="icon-feather-edit"></i>
                                    </a>

                                    @if ($product->status == 9 || $product->status == 4)
                                        <a href="{{ url('productRefresh/'. $product->id) }}"
                                           class="button gray ripple-effect"
                                           title="Активировать" data-tippy-placement="top">
                                            <i class="icon-material-outline-redo"></i>
                                        </a>
                                    @else
                                        <a href="#"
                                           onclick="document.getElementById('deActivate-{{$product->id}}').submit()"
                                           class="button gray ripple-effect ico" title="Деактивировать"
                                           data-tippy-placement="top">
                                            <i class="icon-feather-trash-2"></i>
                                        </a>
                                        <form action="{{ route('ownProducts.destroy', $product->id) }}" method="POST"
                                              id="deActivate-{{$product->id}}">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                        </form>
                                    @endif
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        {{ $products->links() }}
    </div>
@endsection
