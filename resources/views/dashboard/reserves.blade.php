@extends('dashboard.layouts.app')

@section('title')
    Резервы
@endsection

@section('content')

    <div class="dashboard-headline">
        <h3>Резервы</h3>
    </div>

    <!-- Row -->
    <div class="row">

        <!-- Dashboard Box -->
        <div class="col-xl-12">
            <div class="listings-container">
                @foreach ($reserves as $item)

                    <div class="job-listing-details">
                        <div class="job-listing-description">
                            <a href="{{ LaravelLocalization::getLocalizedURL($locale, 'product/'.$item->product->slug) }}"
                               class="job-listing-title">
                                {{ $item->type->title }} за продукт {{ $item->product->title }}
                            </a>
                            <p class="job-listing-text">
                                Сумма: <i class="icon-material-outline-account-balance-wallet"></i>
                                <?= number_format($item->total, 2, ".", " ") . ' ' . $item->currency->code ?>
                            </p>
                            <p class="job-listing-text">
                                Процент:
                                <?= number_format($item->prosent, 2, ".", " ") . ' %' ?>
                            </p>
                            <p class="job-listing-text">
                                Сумма залога:
                                <?= number_format($item->summa, 2, ".", " ") . ' ' . $item->currency->code ?>
                            </p>
                            <p class="job-listing-text">
                                <i class="icon-material-outline-access-time"></i> {{ $item->created_at }}
                            </p>
                        </div>
                    </div>

                @endforeach

            </div>
        </div>
        {{ $reserves->links() }}
    </div>

@endsection