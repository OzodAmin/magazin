@extends('layouts.app')

@section('title')
    Заявка на лот № {{ $notification->product->lot_number }}
@endsection

@section('content')
<div class="single-page-header" data-background-image="{{ asset('front/images/single-job.jpg') }}">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="single-page-header-inner">
                    <div class="left-side">
                        <div class="header-image">
                            {{ Html::image('uploads/product/admin_'.$notification->product->featured_image) }}
                        </div>
                        <div class="header-details">
                            <h3>Заявка на лот № {{ $notification->product->lot_number }}</h3><br>
                            <h3>{{ $notification->product->title }}</h3>
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="icon-material-outline-business"></i>
                                        {{ $notification->product->user->company_legal_name }}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="right-side">
                        <div class="salary-box">
                            <div class="salary-amount">
                                <?= number_format($notification->product->price,2); ?> {{ $notification->product->currency->code }}/{{ $notification->product->measureTable->title_short }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Page Content ================================== -->
<div class="container">
    <div class="row">
        
        <!-- Content -->
        <div class="col-xl-8 col-lg-8 content-right-offset">

            <div class="single-page-section">
                <h3 class="margin-bottom-25">Требуемое количество</h3>
                <p>{{ $notification->product_quantity }} {{ $notification->product->measureTable->title_short }}</p>

                <h3 class="margin-bottom-25">Регион доставки</h3>
                <p>{{ $notification->city->title }}</p>

                <h3 class="margin-bottom-25">Оплата в банковских днях</h3>
                <p>{{ $notification->pay_date }}</p>

                <h3 class="margin-bottom-25">Product Description</h3>
                <p>{{ $notification->product->description }}</p>

                <h3 class="margin-bottom-25">Product Requirements</h3>
                <p>{{ $notification->product->conditions }}</p>
            </div>
        </div>
        

        <!-- Sidebar -->
        <div class="col-xl-4 col-lg-4">
            <div class="sidebar-container">
                @if(auth()->id() != $notification->product->user->id)
                <a href="#small-dialog" class="button full-width button-sliding-icon apply-now-button popup-with-zoom-anim ripple-effect">Предложит цену <i class="icon-material-outline-arrow-right-alt"></i></a>
                @else
                <h3>Это Ваш продукт</h3>
                @endif
                
                    
                <!-- Sidebar Widget -->
                <div class="sidebar-widget">
                    <div class="job-overview">
                        <div class="job-overview-inner">
                            <ul>
                                <li>
                                    <i class="icon-material-outline-business"></i>
                                    <span>Производитель</span>
                                    <h5>{{ $notification->product->manufacturer_title }}</h5>
                                </li>
                                <li>
                                    <i class="icon-material-outline-location-on"></i>
                                    <span>Страна производитель</span>
                                    <h5>{{ $notification->product->country->title }}</h5>
                                </li>
                                <li>
                                    <i class="icon-material-outline-access-time"></i>
                                    <span>Срок доставки</span>
                                    <h5>{{ $notification->product->basis_day }}</h5>
                                </li>
                                <li>
                                    <i class="icon-material-outline-access-time"></i>
                                    <span>Date Posted</span>
                                    <h5>{{ $notification->product->created_at }}</h5>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>

<!-- Leave a Review for Freelancer Popup
================================================== -->
<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">
    <!--Tabs -->
    <div class="sign-in-form">
        <div class="popup-tabs-container">
            <div class="popup-tab-content">
                
                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>Заявка на лот № {{ $notification->product->lot_number }}</h3>
                    <p>Размер залога: <?= $notification->product->price / 100 * $notification->product->deposit .' '. $notification->product->currency->code; ?></p>
                </div>
                    
                <!-- Form -->
                {!! Form::open(['method' => 'POST','url' => 'price-offer', 'onsubmit' => 'return validateForm()']) !!}

                    <strong>Товар</strong>
                    {!! Form::select('productId', 
                        $productArray, 
                        null, 
                        [
                            'class' => 'selectpicker with-border',
                            'data-live-search' => 'true',
                            'data-size' => '7',
                            'title' => '-- SELECT --',
                            'required' => 'true',
                        ]) !!}

                    <input name="requestId" type="hidden" value="{{ $notification->id }}">
                    <input type="hidden" name="requiredQuantity" value="{{ $notification->product_quantity }}">

                    <strong>Цена за товар</strong>
                    <input class="with-border" id="price" onkeypress="javascript:return isNumber(event)" placeholder="Цена за товар" name="user_product_price" type="text" required>

                    <button class="button full-width button-sliding-icon ripple-effect" type="submit">
                        Предложит цену <i class="icon-material-outline-arrow-right-alt"></i>
                    </button>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
            return false;
        return true;
    }

    function validateForm() {

    }
</script>
@endsection