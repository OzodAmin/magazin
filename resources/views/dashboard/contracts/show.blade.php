<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Контракт: {{ $contract->docN }}</title>
    <link rel="stylesheet" href="{{ asset('front/css/invoice.css') }}">
</head>

<body>
<!-- Invoice -->
<div id="invoice">
    <!-- Header -->
    <div class="row">
        <div class="col-xl-4">
            <div id="logo"><img src="{{ asset('front/img/logo.png') }}" alt="IMG-LOGO"></div>
        </div>

        <div class="col-xl-2">
                <a href="{{ url('contracts') }}" class="button ripple-effect">Назад</a>
        </div>

        <div class="col-xl-2">
            @if($contract->status != 4)
                <a href="{{ url('contractApprove/'.$contract->id) }}" class="button ripple-effect">Одобрить</a>
            @else
                <form action="{{ url('contractLoad') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="doc" value="{{ $contract->docN }}">
                    <button type="submit" class="print-button">Download</button>
                </form>
            @endif
        </div>

        <div class="col-xl-4">

            <p id="details">
                <strong>Контракт:</strong> {{ $contract->docN }} <br>
                <strong>Создан:</strong> {{ Carbon\Carbon::parse($contract->created_at)->format('d/m/Y') }}<br>
            </p>
        </div>
    </div>


    <!-- Client & Supplier -->
    <div class="row">

        <div class="col-xl-6">
            <strong class="margin-bottom-5">Supplier</strong>
            <p>
                {{ $contract->seller->company_legal_name }}<br>
                {{ $contract->seller->address }}<br>
                {{ $contract->s_bank_name }} <br>
                {{ $contract->s_bank_account }} <br>
                {{ $contract->s_inn }} <br>
            </p>
        </div>

        <div class="col-xl-6">
            <strong class="margin-bottom-5">Customer</strong>
            <p>
                {{ $contract->buyer->company_legal_name }}<br>
                {{ $contract->buyer->address }} <br>
                {{ $contract->b_bank_name }}<br>
                {{ $contract->b_bank_account }} <br>
                {{ $contract->b_inn }} <br>
            </p>
        </div>
    </div>


    <!-- Invoice -->
    <div class="row">
        <div class="col-xl-12">
            <table class="margin-top-20">
                <tr>
                    <th class="col-xl-6">Description</th>
                    <th class="col-xl-2">Price</th>
                    <th class="col-xl-2">Quantity</th>
                    <th class="col-xl-2">Total</th>
                </tr>

                <tr>
                    <td class="col-xl-6">
                        <a href="{{ LaravelLocalization::getLocalizedURL($locale, 'product/'.$contract->product->translate($locale)->slug) }}"
                           class="button gray">
                            {{ $contract->product->title }}
                        </a>
                    </td>
                    <td class="col-xl-2"><?= number_format($contract->product_price, 2, '.', ' ')?> {{ $contract->currency->code }}</td>
                    <td class="col-xl-2">{{ $contract->quantity.' '.$contract->measure->title_short }}</td>
                    <td class="col-xl-2"><?= number_format($contract->summa, 2, '.', ' ')?> {{ $contract->currency->code }}</td>
                </tr>
            </table>
        </div>

        <div class="col-xl-12">
            <table id="totals">
                <tr>
                    <th>Срок оплаты</th>
                    <th>{{ $contract->pay_date  }} банковских дней</th>
                </tr>
                <tr>
                    <th>Total Due</th>
                    <th><span>$58.80</span></th>
                </tr>
            </table>
        </div>
    </div>
</div>
</body>
</html>
