@extends('dashboard.layouts.app')

@section('content')

    @if (count($errors) > 0)
        <div class="notification error closeable">
            @foreach ($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
            <a class="close" href="#"></a>
        </div>
    @endif

    <!-- Dashboard Headline -->
    <div class="dashboard-headline">
        <h3>Контракты</h3>

        <!-- Breadcrumbs -->
        <nav id="breadcrumbs" class="dark">
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="{{ url('dashboard') }}">Dashboard</a></li>
                <li>Контракты</li>
            </ul>
        </nav>
    </div>

    <!-- Row -->
    <div class="row">
        <!-- Dashboard Box -->
        <div class="col-xl-12">
            <div class="dashboard-box margin-top-0">
                <div class="content">
                    <ul class="dashboard-box-list">
                        @foreach ($contracts as $contract)
                        <li>
                            <!-- Job Listing -->
                            <div class="job-listing">

                                <!-- Job Listing Details -->
                                <div class="job-listing-details">

                                    <!-- Details -->
                                    <div class="job-listing-description">
                                        <h3 class="job-listing-title">
                                            Контракт № {{ $contract->docN }}
                                            <span class="dashboard-status-button green">{{ $contract->statusTable->name }}</span>
                                        </h3>

                                        <!-- Job Listing Footer -->
                                        <div class="job-listing-footer">
                                            <ul>
                                                <li>
                                                    <i class="icon-material-outline-date-range"></i> Created at {{ $contract->created_at }}
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <!-- Buttons -->
                            <div class="buttons-to-right always-visible">
                                <a href="{{ url('contract/'.$contract->id) }}" class="button ripple-effect">View</a>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        {{ $contracts->links() }}
    </div>
@endsection
