@extends('dashboard.layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('datePicker/css/bootstrap-datepicker3.css') }}">
@endsection

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

@section('content')


    <div class="dashboard-headline">
        <nav id="breadcrumbs" class="dark">
            <ul>
                <li>
                    <a href="{{ route('messages') }}">
                        Входящие сообщения
                    </a><br><a href="{{ route('messages.sent') }}">
                        Отправленные сообщения
                    </a><br>
                    <a href="{{ route('messages.create') }}">
                        Новое сообщение
                    </a>
                </li>
                <li><a href="/messages">Непрочитанных @include('dashboard.messenger.unread-count')</a></li>
            </ul>
        </nav>
    </div>

    <h1>

        @if(Route::current()->getName() == 'messages')
            Входящие сообщения
            <?php $route = 'messages'?>
        @else
            <?php $route = 'sent'?>
            Отправленные Сообщения
        @endif
    </h1>
    <br>
    {!! Form::open(['method'=>'GET','url'=>Request::fullUrl(),'class'=>'navbar-form navbar-left','role'=>'search', 'autocomplete' => 'off'])  !!}

    <div class="input-group custom-search-form">

    </div>
    <?php //var_dump($document_type)?>
    <div class="form-group">
        {!! Form::select('subject', $document_type, $request->subject,
    ['placeholder' => 'Тема']); !!}

    </div>
    <div class="form-group">
        {{ Form::text('doc_date',
                               $request->doc_date,
                               [ 'id' => 'datePicker', 'placeholder' => 'От'])
                           }}
    </div>
    <input type="text" class="form-control" name="doc_number" placeholder="№ Договора" value=<?=$request->doc_number?>>

    <input type="text" class="form-control" name="status" placeholder="Статус" value=<?=$request->status?>>

    @if(Auth::user()->hasRole('mod'))

        @if($route == 'messages')
    <div class="form-group">
        {!! Form::select('sender', $users_list, $request->sender,
    [ 'placeholder' => 'Отправитель']); !!}
    </div>
    @else
    <div class="form-group">
        {!! Form::select('receipent', $users_list, $request->receipent,
    [ 'placeholder' => 'Получатель']); !!}
    </div><br>
        @endif

    @endif


    <span class="btn-primary">
        <button class="btn btn-default" type="submit">
            <i class="fa fa-search">Поиск</i>
        </button>
        <button class="btn btn-default">
            <a href="{{ $route }}">
                        Сбросить
                    </a>
        </button>
        </span>
    {!! Form::close() !!}



    <table class="basic-table">
        <thead>
        <tr>
            <th>Тема</th>
            <th>Дата</th>
            <th>Отправитель</th>
            <th>Получатель</th>
            <th>Статус</th>
            <th>№ Договора</th>
        </tr>
        </thead>
        <tbody>

        @each('dashboard.messenger.partials.thread', $threads, 'thread', 'dashboard.messenger.partials.no-threads')


        </tbody>
    </table>
    <br>
    {{ $threads->appends($_GET)->links() }}<br>





    @include('dashboard.messenger.partials.flash')


@stop
@section('scripts')
    <script src="{{ asset('datePicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('datePicker/locales/bootstrap-datepicker.ru.min.js') }}"></script>

    <script>

        $('#datePicker').datepicker({
            format: "yyyy-mm-dd",
            weekStart: 1,
            clearBtn: true,
            autoclose: true,
            language: "{{ str_replace('_', '-', app()->getLocale()) }}"
        });

    </script>
@endsection
