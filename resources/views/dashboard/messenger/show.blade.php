@extends('dashboard.layouts.app')

@section('content')
    <div class="dashboard-headline">
        <nav id="breadcrumbs" class="dark">
            <ul>
                <li>
                    <a href="{{ route('messages') }}">
                        Входящие сообщения
                    </a><br><a href="{{ route('messages.sent') }}">
                        Отправленные сообщения
                    </a><br>
                </li>
            </ul>
        </nav>
    </div>
    <div class="col-md-6">
        <h1>{{ $thread->subjectName->name }} по Договору №{{$thread->doc_number}} от {{$thread->doc_date}}</h1>
        @each('dashboard.messenger.partials.messages', $thread->messages, 'message')

        @include('dashboard.messenger.partials.form-message')
    </div>
@stop
