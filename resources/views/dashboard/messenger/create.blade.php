@extends('dashboard.layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('datePicker/css/bootstrap-datepicker3.css') }}">
@endsection

@section('content')
    <div class="dashboard-headline">
        <nav id="breadcrumbs" class="dark">
            <ul>
                <li>
                    <a href="{{ route('messages') }}">
                        Входящие сообщения
                    </a><br><a href="{{ route('messages.sent') }}">
                        Отправленные сообщения
                    </a><br>
                </li>
            </ul>
        </nav>
    </div>
    <h1>Новое сообщение</h1><br>
    {!! Form::open(['method' => 'POST','route' => 'messages.store', 'files' => true]) !!}
        {{ csrf_field() }}
        <div class="col-md-6">
            <!-- Subject Form Input -->
            <div class="form-group">

                    <h4 class="control-label">Получатель</h4>
                    {!! Form::select('recipients[]',
                        $users,
                        null,
                        [
                            'class' => 'selectpicker with-border',
                            'data-live-search' => 'true',
                            'data-size' => '7',
                            'title' => '-- ВЫБРАТЬ --',
                        ]) !!}

                <label class="control-label">Тема</label>
                {!! Form::select('subject',
                        $document_type,
                        null,
                        [
                            'class' => 'selectpicker with-border',
                            'data-live-search' => 'true',
                            'data-size' => '7',
                            'title' => '-- ВЫБРАТЬ --',
                        ]) !!}



            </div>



            <div class="row">
                <div class="element1 col-md-5{{ $errors->has('doc_date') ? ' has-error' : '' }}">
                <h4 class="control-label">№ Договора</h4>
                <input type="text" class="form-control" name="doc_number"
                       value="{{ old('doc_number') }}">

                </div>

                <div class="element2 col-md-7{{ $errors->has('doc_date') ? ' has-error' : '' }}">
                    <h4 class="control-label">От</h4>

                    <div class="input-with-icon {{ $errors->has('doc_date') ? ' has-error' : '' }}">
                        {{ Form::text('doc_date',
                            null,
                            ['class' => 'with-border', 'id' => 'datePicker'])
                        }}
                        <i class="icon-line-awesome-calendar"></i>
                    </div>
                </div>
            </div>





            <!-- Message Form Input -->
            <div class="form-group">
                <label class="control-label">Описание</label>
                <textarea name="message" class="form-control">{{ old('message') }}</textarea>
            </div>
            <label class="uploadButton-button ripple-effect" for="attachment">Файл</label>
            {!! Form::file('attachment',
                                    [
                                        'class' => 'uploadButton-input',
                                        'accept' => 'jpg,jpeg,png,pdf',
                                        'id' => 'attachment'
                                    ])
                                !!}

            <p class="error-text">
                <?php echo $errors->first('attachment'); ?>
            </p>

        {{--@if($users->count() > 0)--}}

            {{--@endif--}}





            <!-- Submit Form Input -->
            <div class="form-group">
                <button type="submit" class="btn btn-primary form-control">Отправить</button>
            </div>
        </div>
    {!! Form::close() !!}
@stop
@section('scripts')
    <script src="{{ asset('datePicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('datePicker/locales/bootstrap-datepicker.ru.min.js') }}"></script>

    <script>

        $('#datePicker').datepicker({
            format: "yyyy-mm-dd",
            weekStart: 1,
            clearBtn: true,
            autoclose: true,
            language: "{{ str_replace('_', '-', app()->getLocale()) }}"
        });

    </script>
@endsection
