<?php $class = $thread->isUnread(Auth::id()) ? 'alert-info' : ''; ?>


<tr>

            <td><a href="{{ route('messages.show', $thread->id) }}">{{ $thread->subjectName->name }}</a>
                (Непрочитанных: {{ $thread->userUnreadMessagesCount(Auth::id()) }})
                {{--{{ $thread->latestMessage->body }})--}}
            </td>

            <td>{{ $thread->updated_at }}</td>
            <td>{{ $thread->creator()->name }}</td>
            <td>{{ $thread->participantsString($thread->creator()->id) }}</td>
            <td>На рассмотрении</td>
            <td>{{$thread->doc_number}}</td>

</tr>
