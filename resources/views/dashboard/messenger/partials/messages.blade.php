<div class="media">
    <a class="pull-left" href="#">
        <img src="//www.gravatar.com/avatar/{{ md5($message->user->email) }} ?s=64"
             alt="{{ $message->user->name }}" class="img-circle">
    </a>


    <div class="media-body">
        <h5 class="media-heading">{{ $message->user->name }}</h5>
        <p>{{ $message->body }}</p>
        <?php if($message->attachment ): ?>
        Файл: {{ Html::link('uploads/chat_attachments/'.$message->attachment, $message->attachment) }}
        <?php endif; ?>
        <div class="text-muted">
            <small>  {{ $message->created_at->diffForHumans() }}</small>
        </div>
    </div>
</div>