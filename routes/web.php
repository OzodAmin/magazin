<?php

use App\Events\MessagePosted;
use App\Message;
use Illuminate\Support\Facades\Auth;

Route::group(
[
	'prefix' => LaravelLocalization::setLocale(),
	'middleware' => [ 'localize', 'localizationRedirect', 'activity' ]
],
function()
{
    Route::get('/', 'HomeController@index');
    Route::get('contractProve/{id}', ['as'=> 'contractProve', 'uses' => 'HomeController@contractProve']);
    Route::get('categories', 'HomeController@categoryPage');
    Route::get('profile', 'HomeController@profile');
	Route::get('profileEdit', 'HomeController@profileEdit');
    Route::get('api/getCities','HomeController@getCities');
    Route::get('api/getDistricts','HomeController@getDistricts');
    Route::post('registeration', 'HomeController@registeration');
    Auth::routes(['verify' => true]);

    Route::get('products', 'ProductFrontController@index');
    Route::get('product/{slug}', ['as'=> 'product.show', 'uses' => 'ProductFrontController@show']);
    Route::get('category/{id}', 'ProductFrontController@showByCategory');
    Route::post('price-request', 'ProductFrontController@priceRequest');
    Route::post('price-offer', 'ProductFrontController@priceOffer');

    Route::get('dashboard', 'Dashboard\DashboardController@index');
    Route::get('reserves', 'Dashboard\DashboardController@reserves');
    Route::get('contracts', 'Dashboard\ContractController@index');
    Route::get('contract/{id}', 'Dashboard\ContractController@view');
    Route::post('contractLoad', 'Dashboard\ContractController@contractLoad');
    Route::get('contractApprove/{id}', 'Dashboard\ContractController@approve');
    Route::get('notification/{id}', 'Dashboard\DashboardController@notification');
    Route::resource('ownProducts','Dashboard\ProductController');
    Route::get('productRefresh/{id}', 'Dashboard\ProductController@refresh');
    Route::resource('rkps', 'Dashboard\RkpController');

    Route::get('api/getChildCategories','Dashboard\ProductController@getChildCategories');
    Route::get('api/getUserSaldo','Dashboard\ProductController@getUserSaldo');
});

Route::group(['prefix' => 'backend','middleware' => ['role:admin', 'activity']], function() {

	Route::get('/', 'Admin\Backend');

    Route::resource('roles','Admin\RoleController');
    Route::resource('users','Admin\UserController');
    Route::resource('permissions','Admin\PermissionController');

    Route::resource('rkpsAdmin', 'Admin\RkpBackendController');
    Route::resource('payments', 'Admin\PaymentController');

    Route::resource('cities', 'Admin\CityController');
    Route::resource('districts', 'Admin\DistrictController');

    Route::resource('categories', 'Admin\CategoryController');
    Route::resource('childCategories', 'Admin\CategoryChildController');

    Route::resource('measures', 'Admin\MeasureController');
    Route::resource('currencies', 'Admin\CurrencyController');
    Route::resource('basises', 'Admin\BasisController');
    Route::resource('products', 'Admin\ProductController');

    Route::get('contracts', ['as' => 'contracts.index', 'uses' => 'Admin\ContractsController@index']);
    Route::get('contractStatuses', ['as' => 'contractStatuses.index', 'uses' => 'Admin\ContractStatusesController@index']);
    Route::post('docPayments', ['as' => 'docPayment.create','uses' => 'Admin\PaymentController@docPaymentCreate']);

    Route::resource('holidays', 'Admin\HolidaysController');
    Route::resource('statuses', 'Admin\StatusController');
    Route::get('params', ['as' => 'params.index', 'uses' => 'Admin\SystemParamsController@index']);
    Route::put('params', ['as' => 'params.update', 'uses' => 'Admin\SystemParamsController@update']);

    Route::get('api/getDistricts','Admin\UserController@getDistricts');
    Route::get('users/reset/{id}','Admin\UserController@reset');

});
Route::group(['prefix' => 'messages', 'middleware'=> ['activity']], function () {
    Route::get('/', ['as' => 'messages', 'uses' => 'Dashboard\MessagesController@index']);
    Route::get('/sent', ['as' => 'messages.sent', 'uses' => 'Dashboard\MessagesController@sent']);
    Route::get('create', ['as' => 'messages.create', 'uses' => 'Dashboard\MessagesController@create']);
    Route::post('/', ['as' => 'messages.store', 'uses' => 'Dashboard\MessagesController@store']);
    Route::get('{id}', ['as' => 'messages.show', 'uses' => 'Dashboard\MessagesController@show']);
    Route::put('{id}', ['as' => 'messages.update', 'uses' => 'Dashboard\MessagesController@update']);

});
